﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using log4net.Config;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.Infrastructure.Common;
using Pci.CvBuilder.Utility.Encryption;
using Pci.CvBuilder.ApplicationServices.UIPageServices;
using Pci.CvBuilder.ApplicationServices.UIConfigServices;
using Pci.CvBuilder.ApplicationServices.UIMenuAssignmentServices;
using Pci.CvBuilder.ApplicationServices.UIUserServices;
using Pci.CvBuilder.ApplicationServices.UIEducationServices;
using Pci.CvBuilder.ApplicationServices.UIExperienceServices;
using SimpleInjector;
using SimpleInjector.Integration.Wcf;
using Pci.CvBuilder.ApplicationRepository.UserRepo;
using Pci.CvBuilder.ApplicationServices.UIUserGroupServices;
using Pci.CvBuilder.ApplicationServices.UITrainingServices;
using Pci.CvBuilder.ApplicationServices.UISkillServices;
using Pci.CvBuilder.ApplicationServices.UIDocumentServices;
using Pci.CvBuilder.ApplicationServices.UIPersonalInformationServices;

namespace Pci.CvBuilder.ApplicationServices
{
    public class ApplicationService : IApplicationService
    {
        private ServiceHost[] svcHosts;
        private ILogger logger;

        public ApplicationService()
        {
            this.logger = new Logging();
            this.logger.Init(this.GetType().FullName);
        }

        public void Start()
        {
            try
            {
                XmlConfigurator.Configure();

                if (svcHosts != null && svcHosts.Length > 0)
                {
                    ClosingAllHost();
                }

                ConfigFile.ReadEfConnectionString("CvBuilderEntities");

                WcfInjectorBuilder.Build();
                ScopedInjectorBuilder.Build();

                var generatedHost = new List<ServiceHost>();
                generatedHost.Add(new UIPageServiceFactory().Create());
                generatedHost.Add(new UIUserServiceFactory().Create());
                generatedHost.Add(new UIMenuAssignmentServiceFactory().Create());
                generatedHost.Add(new UIConfigServiceFactory().Create());
                generatedHost.Add(new UIUserGroupFactory().Create());
                generatedHost.Add(new UIEducationFactory().Create());
                generatedHost.Add(new UIExperienceFactory().Create());
                generatedHost.Add(new UITrainingFactory().Create());
                generatedHost.Add(new UISkillFactory().Create());
                generatedHost.Add(new UIDocumentFactory().Create());
                generatedHost.Add(new UIPersonalInformationFactory().Create());
                
                svcHosts = generatedHost.ToArray();
                OpeningAllHost();

                //var container = ScopedInjectorBuilder.Get();
                //using (container.BeginLifetimeScope())
                //{
                //    var reference = container.GetInstance<IUserRepository>();
                //    var ls = reference.GetUserList().ToList();
                //}
            }
            catch(Exception e)
            {
                logger.LogOnError("Application Service Start Up", e);
            }
        }

        public void Stop()
        {
            try
            {
                ClosingAllHost();
            }
            catch(Exception e)
            {
                logger.LogOnError("Application Service Stop", e);
            }
        }

        private void OpeningAllHost()
        {
            foreach (ServiceHost svcHost in svcHosts)
            {
                svcHost.Open();
            }
        }

        private void ClosingAllHost()
        {
            for (int i = 0; i <= svcHosts.Length; i++)
            {
                svcHosts[i].Close();
                svcHosts[i] = null;
            }
        }
    }
}
