﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pci.CvBuilder.ApplicationServices
{
    public interface IApplicationService
    {
        void Start();
        void Stop();
    }
}
