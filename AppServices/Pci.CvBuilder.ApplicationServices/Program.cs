﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Pci.CvBuilder.ApplicationServices
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                HostFactory.Run(cfg =>
                    {
                        cfg.SetServiceName("CVBuilder_ApplicationService");
                        cfg.SetDisplayName("CVBuilder_ApplicationService");
                        cfg.SetDescription("");
                        cfg.RunAsLocalService();
                        cfg.Service<IApplicationService>(
                            scfg =>
                            {
                                scfg.ConstructUsing(() => new ApplicationService());
                                scfg.WhenStarted(svc => svc.Start());
                                scfg.WhenStopped(svc => svc.Stop());
                            });
                    });
                Console.ReadLine();
            }
            catch
            {
                throw;
            }
        }
    }
}
