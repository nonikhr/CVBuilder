﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleInjector;
using SimpleInjector.Extensions.LifetimeScoping;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.EFRepositories.EntityFramework.Repository;
using Pci.CommonLib.EFRepositories.EntityFramework.UnitOfWorks;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.Infrastructure.Common;
using Pci.CvBuilder.Entity.Provider;
using Pci.CvBuilder.ApplicationRepository.UserRepo;
using Pci.CvBuilder.ApplicationRepository.ConfigurationParameterRepo;
using Pci.CvBuilder.ApplicationRepository.MenuAssignmentRepo;

namespace Pci.CvBuilder.ApplicationServices
{
    public class ScopedInjectorBuilder
    {
        private static Container container;

        public static void Build()
        {
            container = new Container();
            container.Options.DefaultScopedLifestyle = new LifetimeScopeLifestyle();
            container.Register<IContextProvider, ContextProvider>(Lifestyle.Scoped);
            container.Register<ILogger, Logging>(Lifestyle.Scoped);
            container.Register<IRepositoryTyped, RepositoryTyped>(Lifestyle.Scoped);
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
            container.RegisterConditional(typeof(IRepository<>),
               typeof(Repository<>),
               Lifestyle.Transient,
               c => !c.Handled);

            //container.Register<IUserRepository, UserRepository>(Lifestyle.Transient);
            //container.Register<IConfigurationParameterRepository, ConfigurationParameterRepository>(Lifestyle.Transient);
            //container.Register<IMenuAssignmentRepository, MenuAssignmentRepository>(Lifestyle.Transient);

            container.Verify();
        }

        public static Container Get()
        {
            return container;
        }
    }
}
