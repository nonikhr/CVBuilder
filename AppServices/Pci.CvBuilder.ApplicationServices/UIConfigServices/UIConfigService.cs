﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CvBuilder.Entity;
using Pci.CvBuilder.ApplicationRepository.ConfigurationParameterRepo;
using Pci.CommonLib.Infrastructure.Logging;

namespace Pci.CvBuilder.ApplicationServices.UIConfigServices
{
    public class UIConfigService : IUIConfigurationParameterServices
    {
        //private readonly IRepository<Config> repository;
        private readonly IUnitOfWork uow;
        private readonly IRepositoryTyped logRepository;
        private readonly IConfigurationParameterRepository configRepository;

        public UIConfigService(
            IConfigurationParameterRepository configRepository,
            ILogger logger,
            IUnitOfWork uow,
            IRepositoryTyped logRepository)
        {
            this.configRepository = configRepository;
            this.logRepository = logRepository;
            this.uow = uow;
        }

        /// <summary>
        /// Get parameter configuration
        /// </summary>
        /// <returns></returns>
        public string GetParameterConfiguration()
        {
            string jsonData = string.Empty;
            var objConfig = configRepository.GetParameterConfiguration();

            return JsonConvert.SerializeObject(objConfig,
               Formatting.None,
               new JsonSerializerSettings
               {
                   ReferenceLoopHandling = ReferenceLoopHandling.Ignore
               });
        }

        /// <summary>
        /// Update config
        /// </summary>
        /// <param name="config"></param>
        public void UpdateConfig(Entity.Config config)
        {
            try
            {
                this.configRepository.Update(config);
                this.uow.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
    }
}
