﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Configuration;
using Pci.CommonLib.ChannelFactory;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.ApplicationServices.UIUserServices;

namespace Pci.CvBuilder.ApplicationServices.UIConfigServices
{
    public class UIConfigServiceFactory
    {
        public ServiceHost Create()
        {
            string strAdrTcp = ConfigurationManager.AppSettings["UICofigServiceUrl"];

            return new ServicesFactory().CreateWithInjector(WcfInjectorBuilder.Get(),
                typeof(IUIConfigurationParameterServices),
                typeof(UIConfigService),
                strAdrTcp);
        }
    }
}
