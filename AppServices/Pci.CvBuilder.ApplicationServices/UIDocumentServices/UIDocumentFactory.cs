﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Configuration;
using Pci.CommonLib.ChannelFactory;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.ApplicationServices.UIDocumentServices;

namespace Pci.CvBuilder.ApplicationServices.UIDocumentServices
{
    public class UIDocumentFactory
    {
        public ServiceHost Create()
        {
            string strAdrTcp = ConfigurationManager.AppSettings["UIDocumentServiceUrl"];
            return new ServicesFactory().CreateWithInjector(WcfInjectorBuilder.Get(),
            typeof(IUIDocumentServices),
            typeof(UIDocumentService),
            strAdrTcp);
        }
    }
}
