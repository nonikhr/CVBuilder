﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.DocumentRepo;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationServices.UIDocumentServices
{
    public class UIDocumentService : IUIDocumentServices
    {
        private readonly IRepositoryTyped logRepository;
        private readonly IDocumentRepository documentRepository;
        private readonly IUnitOfWork uow;

        public UIDocumentService(IDocumentRepository documentRepository,
        ILogger logger,
        IUnitOfWork uow,
        IRepositoryTyped repositoryTyped,
        IRepositoryTyped logRepository)
        {
            this.documentRepository = documentRepository;
            this.logRepository = RepositoryTypedFactory.Create(repositoryTyped, logger);
            this.uow = uow;
        }

        /// <summary>
        /// Create Document
        /// </summary>
        /// <param name="document"></param>
        public void Create(Document document)
        {
            try
            {
                this.documentRepository.Create(document);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update Document
        /// </summary>
        /// <param name="document"></param>
        public void Update(Document document)
        {
            try
            {
                this.documentRepository.Update(document);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete Document
        /// </summary>
        /// <param name="document"></param>
        public void Delete(Document document)
        {
            try
            {
                this.documentRepository.Delete(document);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get List Document
        /// </summary>
        /// <returns></returns>
        public string GetDocumentList()
        {
            string jsonData = string.Empty;
            var objDocumentList = documentRepository.GetDocumentList();
            
            return JsonConvert.SerializeObject(objDocumentList,
               Formatting.None,
               new JsonSerializerSettings
               {
                   ReferenceLoopHandling = ReferenceLoopHandling.Ignore
               });
        }

        /// <summary>
        /// Get Document By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetDocumentById(int id)
        {
            string jsonData = string.Empty;
            var objDocument = documentRepository.GetDocumentById(id);
            jsonData = JsonConvert.SerializeObject(objDocument);
            return jsonData;
        }
    }
}
