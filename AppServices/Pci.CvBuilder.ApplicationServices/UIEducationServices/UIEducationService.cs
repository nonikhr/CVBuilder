﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.EducationRepo;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationServices.UIEducationServices
{
    public class UIEducationService : IUIEducationServices
    {
        private readonly IRepositoryTyped logRepository;
        private readonly IEducationRepository educationRepository;
        private readonly IUnitOfWork uow;

        public UIEducationService(IEducationRepository educationRepository,
        ILogger logger,
        IUnitOfWork uow,
        IRepositoryTyped repositoryTyped,
        IRepositoryTyped logRepository)
        {
            this.educationRepository = educationRepository;
            this.logRepository = RepositoryTypedFactory.Create(repositoryTyped, logger);
            this.uow = uow;
        }

        /// <summary>
        /// Create Education
        /// </summary>
        /// <param name="education"></param>
        public void Create(Education education)
        {
            try
            {
                this.educationRepository.Create(education);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update Education
        /// </summary>
        /// <param name="education"></param>
        public void Update(Education education)
        {
            try
            {
                this.educationRepository.Update(education);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete Education
        /// </summary>
        /// <param name="education"></param>
        public void Delete(Education education)
        {
            try
            {
                this.educationRepository.Delete(education);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get List Education
        /// </summary>
        /// <returns></returns>
        public string GetEducationList()
        {
            try
            {
                string jsonData = string.Empty;
                var objEducationList = educationRepository.GetUserEducationList();

                return JsonConvert.SerializeObject(objEducationList,
                Formatting.None,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get Id Education
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetEducationById(int id)
        {
            string jsonData = string.Empty;
            var objEducation = educationRepository.GetUserEducationById(id);
            jsonData = JsonConvert.SerializeObject(objEducation);
            return jsonData;
        }
    }
}
