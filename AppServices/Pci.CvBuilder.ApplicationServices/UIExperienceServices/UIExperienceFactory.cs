﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Configuration;
using Pci.CommonLib.ChannelFactory;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.ApplicationServices.UIExperienceServices;

namespace Pci.CvBuilder.ApplicationServices.UIExperienceServices
{
    public class UIExperienceFactory
    {
        public ServiceHost Create()
        {

            string strAdrTcp = ConfigurationManager.AppSettings["UIExperienceServiceUrl"];
            return new ServicesFactory().CreateWithInjector(WcfInjectorBuilder.Get(),
            typeof(IUIExperienceServices),
            typeof(UIExperienceService),
            strAdrTcp);

        }
    }
}
