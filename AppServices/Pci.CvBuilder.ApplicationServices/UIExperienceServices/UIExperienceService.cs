﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.ExperienceRepo;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationServices.UIExperienceServices
{
    public class UIExperienceService : IUIExperienceServices
    {
        private readonly IRepositoryTyped logRepository;
        private readonly IExperienceRepository experienceRepository;
        private readonly IUnitOfWork uow;

        public UIExperienceService(IExperienceRepository experienceRepository,
        ILogger logger,
        IUnitOfWork uow,
        IRepositoryTyped repositoryTyped,
        IRepositoryTyped logRepository)
        {
            this.experienceRepository = experienceRepository;
            this.logRepository = RepositoryTypedFactory.Create(repositoryTyped, logger);
            this.uow = uow;
        }

        /// <summary>
        /// Create Experience
        /// </summary>
        /// <param name="education"></param>
        public void Create(Experience experience)
        {
            try
            {
                this.experienceRepository.Create(experience);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update Experience
        /// </summary>
        /// <param name="education"></param>
        public void Update(Experience experience)
        {
            try
            {
                this.experienceRepository.Update(experience);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete Experience
        /// </summary>
        /// <param name="education"></param>
        public void Delete(Experience experience)
        {
            try
            {
                this.experienceRepository.Delete(experience);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get List Experience
        /// </summary>
        /// <returns></returns>
        public string GetExperienceList()
        {
            string jsonData = string.Empty;
            var objExperienceList = experienceRepository.GetExperienceList();

            return JsonConvert.SerializeObject(objExperienceList,
                Formatting.None,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
        }

        /// <summary>
        /// Get Id Experience
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetExperienceById(int id)
        {
            string jsonData = string.Empty;
            var objExperience = experienceRepository.GetExperienceById(id);
            jsonData = JsonConvert.SerializeObject(objExperience);
            return jsonData;
        }
    }
}
