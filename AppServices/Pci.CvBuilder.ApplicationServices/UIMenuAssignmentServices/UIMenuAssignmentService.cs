﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CvBuilder.ApplicationRepository.MenuAssignmentRepo;
using Pci.CvBuilder.ApplicationRepository.Common;


namespace Pci.CvBuilder.ApplicationServices.UIMenuAssignmentServices
{
    public class UIMenuAssignmentService : IUIMenuAssignmentServices
    {
        private readonly IRepository<MenuAsssignmentApproval> repository;
        private readonly IRepository<MenuAssignment> repositoryMenuAssignment;
        private readonly IMenuAssignmentRepository menuAssignmentRepository;
        private readonly IUnitOfWork uow;
        private readonly IRepositoryTyped logRepositoy;

        public UIMenuAssignmentService(IMenuAssignmentRepository menuAssignmentRepository,
            ILogger logger,
            IUnitOfWork uow,
            IRepositoryTyped logRepository,
            IRepositoryTyped repositoryType,
            IRepository<MenuAsssignmentApproval> repository,
            IRepository<MenuAssignment> repositoryMenuAssignment)
        {
            this.menuAssignmentRepository = menuAssignmentRepository;
            this.logRepositoy = RepositoryTypedFactory.Create(repositoryType, logger);
            this.uow = uow;
            this.repository = repository;
            this.repositoryMenuAssignment = repositoryMenuAssignment;
        }

        public void CreateMenuAssignment(MenuAsssignmentApproval model)
        {
            try
            {
                if (this.repository.GetDbSet().Where(o => o.UserGroupId == model.UserGroupId && o.Id != model.Id).Count() > 0)
                {
                    throw new ApplicationException("Data already exists.");
                }

                repository.Create(model);
                this.uow.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public int UpdateMenuAssignment(MenuAsssignmentApproval model)
        {
            try
            {
                if(this.repository.GetDbSet().Where(o=> o.UserGroupId == model.UserGroupId && o.Id != model.Id ).Count()>0)
                {
                    throw new ApplicationException("Data already exists.");
                }

                // remove existing data by menu...
                var dataToRemove = this.repositoryMenuAssignment.GetDbSet().Where(o => o.MenuAssignmentApproval == model.UserGroupId).ToList();
                if (dataToRemove.Count() > 0)
                {
                    foreach (var item in dataToRemove)
                    {
                        this.repositoryMenuAssignment.Delete(item);
                    }
                }

                // create the new menus by id
                var dataToCreate = model.MenuAssignment.ToList();
                if (dataToCreate.Count() > 0)
                {
                    foreach (var item in dataToCreate)
                    {
                        MenuAssignment a = new MenuAssignment()
                        {
                            MenuAssignmentApproval = model.UserGroupId,
                            MenuId = item.MenuId
                        };
                        this.repositoryMenuAssignment.Create(a);
                    }
                }

                //this.repository.Update(model, model.Id, null); 

               return  this.uow.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public int DeleteMenuAssignment(MenuAsssignmentApproval model)
        {
            try
            {
                repository.Delete(model);
                return this.uow.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public string GetMenuByUserGroupId(long userGroupid)
        {
            try
            {
                string strJson = string.Empty;
                var data = menuAssignmentRepository.GetMenuByUserGroupId(userGroupid);
                strJson = JsonConvert.SerializeObject(data,
                    Formatting.None,
                    new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                return strJson;
            }
            catch
            {
                throw;
            }
        }

        //public string GetMenuId(string urlClassName)
        //{
        //    string result = "0";
        //    var objMenuList = menuAssignmentRepository.GetMenuId(urlClassName);
        //    if (objMenuList != null)
        //        result = objMenuList.ToString();
        //    return result;
        //}
    }
}
