﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Configuration;
using Pci.CvBuilder.WcfServices;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.ChannelFactory;


namespace Pci.CvBuilder.ApplicationServices.UIMenuAssignmentServices
{
    public class UIMenuAssignmentServiceFactory
    {
        public ServiceHost Create()
        {
        string strAdrTCP = ConfigurationManager.AppSettings["UIMenuAssignmentServiceUrl"];
        return new ServicesFactory().CreateWithInjector(WcfInjectorBuilder.Get(),
            typeof(IUIMenuAssignmentServices),
            typeof(UIMenuAssignmentService),
            strAdrTCP);
            }
    }
}
