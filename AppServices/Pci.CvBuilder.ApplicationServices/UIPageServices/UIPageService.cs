﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Kendo.Mvc.UI;
using Kendo.Mvc.Infrastructure;
using Kendo.Mvc;
using Newtonsoft.Json;
using System.Data.Entity;
using Pci.CvBuilder.WcfServices;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationServices.UIPageServices
{
    public class UIPageService : IUIPageServices
    {
        private readonly IRepositoryTyped logRepository;

        public UIPageService(
            ILogger logger,
            IRepositoryTyped logRepository,
            IRepositoryTyped repositoryType)
        {
            this.logRepository = RepositoryTypedFactory.Create(repositoryType, logger);
        }

        /// <summary>
        /// Get detail data
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public string GetDetailData(Message.DetailRequest request)
        {
            try
            {
                var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions
                    {
                        IsolationLevel = IsolationLevel.ReadUncommitted
                    });
                using(scope)
                {
                    return logRepository.GetDetail(request.Type, new object[] { request.Id });
                }
            }
            catch
            {
                throw;
            }
        }
        
        public string GetPageData(Message.PageRequest request) 
        {
            try
            {
                var scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,

                    new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    });

                using (scope)
                {
                    var efRequest = new DataSourceRequest();
                    efRequest.Filters = FilterDescriptorFactory.Create(request.Request.Filter);
                    efRequest.Page = request.Request.Page;
                    efRequest.PageSize = request.Request.PageSize;
                    if (!string.IsNullOrEmpty(request.Request.Group))
                    {
                        efRequest.Groups = GridDescriptorSerializer.Deserialize<GroupDescriptor>(request.Request.Group);
                    }
                    if (!string.IsNullOrEmpty(request.Request.Sort))
                    {
                        efRequest.Sorts = GridDescriptorSerializer.Deserialize<SortDescriptor>(request.Request.Sort);
                    }
                    var result = logRepository.GetPageData(request.Type, efRequest);
                    return JsonConvert.SerializeObject(result,
                        Formatting.None,
                        new JsonSerializerSettings
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
                }
            }
            catch
            {
                throw;
            }
        }

        public string GetAllList(Message.ListRequest request)
        {
            return JsonConvert.SerializeObject(logRepository.GetContext()
                .Set(Type.GetType(request.Type)).ToListAsync().Result,
                Formatting.None,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
        }
    }
}
