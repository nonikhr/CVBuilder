﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Configuration;
using Pci.CvBuilder.WcfServices;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.ChannelFactory;

namespace Pci.CvBuilder.ApplicationServices.UIPageServices
{
    public class UIPageServiceFactory 
    {
        public ServiceHost Create()
        {
            string strAdrTcp = ConfigurationManager.AppSettings["UIPageServiceUrl"];

            return new ServicesFactory().CreateWithInjector(WcfInjectorBuilder.Get(),
                typeof(IUIPageServices),
                typeof(UIPageService),
                strAdrTcp);
        }
       
    }
}
