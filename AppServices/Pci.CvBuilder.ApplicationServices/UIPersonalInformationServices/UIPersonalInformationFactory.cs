﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Configuration;
using Pci.CommonLib.ChannelFactory;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.ApplicationServices.UIPersonalInformationServices;

namespace Pci.CvBuilder.ApplicationServices.UIPersonalInformationServices
{
    public class UIPersonalInformationFactory
    {
        public ServiceHost Create(){
        string strAdrTcp = ConfigurationManager.AppSettings["UIPersonalInformationServiceUrl"];
            return new ServicesFactory().CreateWithInjector(WcfInjectorBuilder.Get(),
            typeof(IUIPersonalInformationServices),
            typeof(UIPersonalInformationService),
            strAdrTcp);
    }
    }
}
