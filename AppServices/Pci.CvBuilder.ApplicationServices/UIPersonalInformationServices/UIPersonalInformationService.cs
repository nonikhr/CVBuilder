﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.PersonalInformationRepo;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationServices.UIPersonalInformationServices
{
    public class UIPersonalInformationService : IUIPersonalInformationServices
    {
        private readonly IRepositoryTyped logRepository;
        private readonly IPersonalInformationRepository personalInformationRepository;
        private readonly IUnitOfWork uow;

        public UIPersonalInformationService(IPersonalInformationRepository personalInformationRepository,
            ILogger logger,
            IUnitOfWork uow,
            IRepositoryTyped repositoryTyped,
            IRepositoryTyped logRepository)
        {
            this.personalInformationRepository = personalInformationRepository;
            this.logRepository = RepositoryTypedFactory.Create(repositoryTyped, logger);
            this.uow = uow;
        }

        /// <summary>
        /// Create Personal Information
        /// </summary>
        /// <param name="personalInformation"></param>
        //public void Create(PersonalInformation personalInformation)
        //{
        //    try
        //    {
        //        this.personalInformationRepository.Create(personalInformation);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}


        public void Update(User user)
        {
            try
            {
                this.personalInformationRepository.Update(user);
            }
            catch
            {
                throw;
            }
        }
    }
}
