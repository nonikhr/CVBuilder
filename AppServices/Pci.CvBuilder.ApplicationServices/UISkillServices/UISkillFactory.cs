﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Configuration;
using Pci.CommonLib.ChannelFactory;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.ApplicationServices.UISkillServices;

namespace Pci.CvBuilder.ApplicationServices.UISkillServices
{
    public class UISkillFactory
    {
        public ServiceHost Create()
        {

            string strAdrTcp = ConfigurationManager.AppSettings["UISkillServiceUrl"];
            return new ServicesFactory().CreateWithInjector(WcfInjectorBuilder.Get(),
            typeof(IUISkillServices),
            typeof(UISkillService),
            strAdrTcp);

        }
    }
}
