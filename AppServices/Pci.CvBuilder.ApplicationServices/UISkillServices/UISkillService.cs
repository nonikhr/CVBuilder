﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.SkillRepo;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationServices.UISkillServices
{
    public class UISkillService : IUISkillServices
    {
        private readonly IRepositoryTyped logRepository;
        private readonly ISkillRepository skillRepository;
        private readonly IUnitOfWork uow;

        public UISkillService(ISkillRepository skillRepository,
            ILogger logger,
            IUnitOfWork uow,
            IRepositoryTyped repositoryTyped,
            IRepositoryTyped logRepository)
        {
            this.skillRepository = skillRepository;
            this.logRepository = RepositoryTypedFactory.Create(repositoryTyped, logger);
            this.uow = uow;
        }

        /// <summary>
        /// Create Skill
        /// </summary>
        /// <param name="skill"></param>
        public void Create(Skill skill)
        {
            try
            {
                this.skillRepository.Create(skill);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update Skill
        /// </summary>
        /// <param name="skill"></param>
        public void Update(Skill skill)
        {
            try
            {
                this.skillRepository.Update(skill);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete Skill
        /// </summary>
        /// <param name="skill"></param>
        public void Delete(Skill skill)
        {
            try
            {
                this.skillRepository.Delete(skill);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get Skill List
        /// </summary>
        /// <returns></returns>
        public string GetSkillList()
        {
            string jsonData = string.Empty;
            var objSkillList = skillRepository.GetSkillList();

            return JsonConvert.SerializeObject(objSkillList,
               Formatting.None,
               new JsonSerializerSettings
               {
                   ReferenceLoopHandling = ReferenceLoopHandling.Ignore
               });
        }

        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetSkillById(int id)
        {
            string jsonData = string.Empty;
            var objSkill = skillRepository.GetSkillById(id);
            jsonData = JsonConvert.SerializeObject(objSkill);
            return jsonData;
        }
    }
}
