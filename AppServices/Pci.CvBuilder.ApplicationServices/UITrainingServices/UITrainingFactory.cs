﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Configuration;
using Pci.CommonLib.ChannelFactory;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.ApplicationServices.UIEducationServices;

namespace Pci.CvBuilder.ApplicationServices.UITrainingServices
{
    public class UITrainingFactory
    {
        public ServiceHost Create()
        {

            string strAdrTcp = ConfigurationManager.AppSettings["UITrainingServiceUrl"];
            return new ServicesFactory().CreateWithInjector(WcfInjectorBuilder.Get(),
            typeof(IUITrainingServices),
            typeof(UITrainingService),
            strAdrTcp);

        }
    }
}
