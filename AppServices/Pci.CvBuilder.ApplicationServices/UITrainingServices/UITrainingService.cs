﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.TrainingRepo;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationServices.UITrainingServices
{
    public class UITrainingService : IUITrainingServices
    {
        private readonly IRepositoryTyped logRepository;
        private readonly ITrainingRepository trainingRepository;
        private readonly IUnitOfWork uow;

        public UITrainingService(ITrainingRepository trainingRepository,
            ILogger logger,
            IUnitOfWork uow,
            IRepositoryTyped repositoryTyped,
            IRepositoryTyped logRepository)
        {
            this.trainingRepository = trainingRepository;
            this.logRepository = RepositoryTypedFactory.Create(repositoryTyped, logger);
            this.uow = uow;
        }

        /// <summary>
        /// Create Training
        /// </summary>
        public void Create(Training training)
        {
            try
            {
                this.trainingRepository.Create(training);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update Training
        /// </summary>
        /// <param name="training"></param>
        public void Update(Training training)
        {
            try
            {
                this.trainingRepository.Update(training);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete Training
        /// </summary>
        /// <param name="training"></param>
        public void Delete(Training training)
        {
            try
            {
                this.trainingRepository.Delete(training);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get List Training
        /// </summary>
        /// <returns></returns>
        public string GetTrainingList()
        {
            string jsonData = string.Empty;
            var objTrainingList = trainingRepository.GetTrainingList();

            return JsonConvert.SerializeObject(objTrainingList,
               Formatting.None,
               new JsonSerializerSettings
               {
                   ReferenceLoopHandling = ReferenceLoopHandling.Ignore
               });
        }

        /// <summary>
        /// Get Id Training
        /// </summary>
        /// <returns></returns>
        public string GetTrainingById(int id)
        {
            string jsonData = string.Empty;
            var objTraining = trainingRepository.GetTrainingById(id);
            jsonData = JsonConvert.SerializeObject(objTraining);
            return jsonData;
        }
    }
}
