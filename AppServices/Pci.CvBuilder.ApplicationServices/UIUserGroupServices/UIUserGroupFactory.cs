﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Configuration;
using Pci.CommonLib.ChannelFactory;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.ApplicationServices.UIUserGroupServices;

namespace Pci.CvBuilder.ApplicationServices.UIUserGroupServices
{
    public class UIUserGroupFactory 
    {
        public ServiceHost Create()
        {
            string strAdrTcp = ConfigurationManager.AppSettings["UIUserGroupServiceUrl"];

            return new ServicesFactory().CreateWithInjector(WcfInjectorBuilder.Get(),
                typeof(IUIUserGroupServices),
                typeof(UIUserGroupService),
                strAdrTcp);
        }
    }
}
