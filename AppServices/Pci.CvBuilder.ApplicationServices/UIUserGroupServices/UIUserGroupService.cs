﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CvBuilder.ApplicationRepository.UserGroupRepo;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationServices.UIUserGroupServices
{
    public class UIUserGroupService : IUIUserGroupServices
    {
        private readonly IRepositoryTyped logRepository;
        private readonly IUserGroupRepository userGroupRepository;
        private readonly IUnitOfWork uow;

        public UIUserGroupService(IUserGroupRepository userGroupRepository,
            ILogger logger,
            IUnitOfWork uow,
            IRepositoryTyped repositoryTyped,
            IRepositoryTyped logRepository)
        {
            this.userGroupRepository = userGroupRepository;
            this.logRepository = RepositoryTypedFactory.Create(repositoryTyped, logger);
            this.uow = uow;
        }

        /// <summary>
        /// Create UserGroup
        /// </summary>
        /// <param name="userGroup"></param>
        public void Create(UserGroup userGroup)
        {
            try
            {
                this.userGroupRepository.Create(userGroup);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update User Group
        /// </summary>
        /// <param name="userGroup"></param>
        public void Update(UserGroup userGroup)
        {
            try
            {
                this.userGroupRepository.Update(userGroup);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete User Group
        /// </summary>
        /// <param name="userGroup"></param>
        public void Delete(UserGroup userGroup)
        {
            try
            {
                this.userGroupRepository.Delete(userGroup);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get user group list
        /// </summary>
        /// <returns></returns>
        public string GetUserGroupList()
        {
            string jsonData = string.Empty;
            var objUserList = userGroupRepository.GetUserGroupList();
            jsonData = JsonConvert.SerializeObject(objUserList);
            return jsonData;
        }

        /// <summary>
        /// Get user group by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetUserGroupById(int id)
        {
            string jsonData = string.Empty;
            var objUserGroup = userGroupRepository.GetUserGroupById(id);
            jsonData = JsonConvert.SerializeObject(objUserGroup);
            return jsonData;
        }
    }
}
