﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CvBuilder.ApplicationRepository.UserRepo;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationServices.UIUserServices
{
    class UIUserService : IUIUserServices
    {
        private readonly IRepository<User> repository;
        private readonly IUnitOfWork uow;
        private readonly IRepositoryTyped logRepository;
        private readonly IUserRepository userRepository;

        public UIUserService(IRepository<User> repository,
            IUserRepository userRepository,
            ILogger logger,
            IUnitOfWork uow,
            IRepositoryTyped logRepository,
            IRepositoryTyped repositoryType)
        {
            this.userRepository = userRepository;
            this.logRepository = RepositoryTypedFactory.Create(repositoryType, logger);
            this.uow = uow;
        }

        /// <summary>
        /// Create user
        /// </summary>
        /// <param name="user"></param>
        public void CreateUser(User user)
        {
            try
            {
                this.userRepository.Create(user);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user"></param>
        public void UpdateUser(User user)
        {
            try
            {
                this.userRepository.Update(user);
                this.uow.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update user log on
        /// </summary>
        /// <param name="user"></param>
        public void UpdateUserLogon(User user)
        {
            //logRepository.Update(logRepository,nul)
            throw new NotImplementedException();
        }

        public void DeleteUser(User user)
        {
            try
            {
                userRepository.Delete(user);
                this.uow.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get user by user Id
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public string GetUserByUserId(string userId, string password)
        {
            string jsonData = string.Empty;
            var objUser = userRepository.GetUserByUserId(userId, password);

            jsonData = JsonConvert.SerializeObject(objUser,
                Formatting.None,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
            return jsonData;
        }

        /// <summary>
        /// Get user by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetUserById(int id)
        {
            string jsonData = string.Empty;
            var objUser = userRepository.GetUserById(id);
            jsonData = JsonConvert.SerializeObject(objUser);
            return jsonData;
        }

        /// <summary>
        /// Get user list
        /// </summary>
        /// <returns></returns>
        public string GetUserList()
        {
            string jsonData = string.Empty;
            var objUserList = userRepository.GetUserList();

            return JsonConvert.SerializeObject(objUserList,
               Formatting.None,
               new JsonSerializerSettings
               {
                   ReferenceLoopHandling = ReferenceLoopHandling.Ignore
               });
        }
    }
}
