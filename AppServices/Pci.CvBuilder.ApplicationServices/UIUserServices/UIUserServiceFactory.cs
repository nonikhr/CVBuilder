﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Configuration;
using Pci.CommonLib.ChannelFactory;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.ApplicationServices.UIUserServices;

namespace Pci.CvBuilder.ApplicationServices.UIUserServices
{
    public class UIUserServiceFactory
    {
        public ServiceHost Create()
        {
            string strAdrTcp = ConfigurationManager.AppSettings["UIUserServiceUrl"];

            return new ServicesFactory().CreateWithInjector(WcfInjectorBuilder.Get(),
                typeof(IUIUserServices),
                typeof(UIUserService),
                strAdrTcp);
        }
    }
}
