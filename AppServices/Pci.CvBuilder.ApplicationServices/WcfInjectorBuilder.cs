﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleInjector;
using SimpleInjector.Integration.Wcf;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.Infrastructure.Common;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.EFRepositories.EntityFramework.Repository;
using Pci.CommonLib.EFRepositories.EntityFramework.UnitOfWorks;
using Pci.CvBuilder.ApplicationRepository.ConfigurationParameterRepo;
using Pci.CvBuilder.ApplicationRepository.UserRepo;
using Pci.CvBuilder.ApplicationRepository.MenuAssignmentRepo;
using Pci.CvBuilder.ApplicationRepository.UserGroupRepo;
using Pci.CvBuilder.ApplicationRepository.EducationRepo;
using Pci.CvBuilder.ApplicationRepository.ExperienceRepo;
using Pci.CvBuilder.ApplicationRepository.TrainingRepo;
using Pci.CvBuilder.ApplicationRepository.SkillRepo;
using Pci.CvBuilder.ApplicationRepository.DocumentRepo;
using Pci.CvBuilder.ApplicationRepository.PersonalInformationRepo;
using Pci.CvBuilder.Entity.Provider;

namespace Pci.CvBuilder.ApplicationServices
{
    public class WcfInjectorBuilder
    {
        private static Container container;

        public static void Build()
        {
            container = new Container();
            container.Options.DefaultScopedLifestyle = new WcfOperationLifestyle();
            container.Register<IContextProvider, ContextProvider>(Lifestyle.Scoped);
            container.Register<ILogger, Logging>(Lifestyle.Scoped);
            container.Register<IRepositoryTyped, RepositoryTyped>(Lifestyle.Scoped);
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
            container.RegisterConditional(typeof(IRepository<>),
              typeof(Repository<>),
              Lifestyle.Transient,
              c => !c.Handled);
            container.Register<IUserRepository, UserRepository>(Lifestyle.Transient);
            container.Register<IConfigurationParameterRepository, ConfigurationParameterRepository>(Lifestyle.Transient);
            container.Register<IMenuAssignmentRepository, MenuAssignmentRepository>(Lifestyle.Transient);
            container.Register<IUserGroupRepository, UserGroupRepository>(Lifestyle.Transient);
            container.Register<IEducationRepository, EducationRepository>(Lifestyle.Transient);
            container.Register<IExperienceRepository, ExperienceRepository>(Lifestyle.Transient);
            container.Register<ITrainingRepository, TrainingRepository>(Lifestyle.Transient);
            container.Register<ISkillRepository, SkillRepository>(Lifestyle.Transient);
            container.Register<IDocumentRepository, DocumentRepository>(Lifestyle.Transient);
            container.Register<IPersonalInformationRepository, PersonalInformationRepository>(Lifestyle.Transient);

            container.Verify();
        }

        public static Container Get()
        {
            return container;
        }
    }
}
