﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.ServiceModel;
using SimpleInjector;
using Pci.CvBuilder.Utility.Wcf.Host;

namespace Pci.CommonLib.ChannelFactory
{
    public class ServicesFactory
    {
        private string certificateName = ConfigurationManager.AppSettings["CertificateName"];

        public ServiceHost CreateWithInjector(Container container,
            Type interfaceType,
            Type implemtationType,
            string strAdrTCP)
        {
            ServiceHost svcHost;

            svcHost = new ServiceHandler().CreateService(interfaceType,
                implemtationType, strAdrTCP, container);

            return svcHost;
        }
    }
}
