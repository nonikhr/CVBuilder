﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Pci.CommonLib.EFRepositories.Interface;

namespace Pci.CommonLib.EFRepositories.EntityFramework.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        internal DbContext context;
        internal DbSet<T> dbSet;

        /// <summary>
        /// Use when need locking on multithreading
        /// </summary>
        protected readonly object Locker = new object();
        public Repository(IContextProvider provider)
        {
            this.context = provider.GetContext();
            this.dbSet = context.Set<T>();
        }

        /// <summary>
        /// Create data
        /// </summary>
        /// <param name="entity"></param>
        public void Create(T entity)
        {
            dbSet.Add(entity);
        }

        /// <summary>
        /// entity to delete
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(T entity)
        {
            if (context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }

        /// <summary>
        /// Delete data
        /// </summary>
        /// <param name="id"></param>
        public void Delete(object id)
        {
            T entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        /// <summary>
        /// update
        /// </summary>
        /// <param name="entity"></param>
        //public void Update(T entity)
        //{
        //    dbSet.Attach(entity);
        //    context.Entry(entity).State = EntityState.Modified;
        //}

        /// <summary>
        /// Get list
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <param name="includeProperties"></param>
        /// <returns></returns>
        public List<T> GetList(System.Linq.Expressions.Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "")
        {
            IQueryable<T> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetById(object id)
        {
            return dbSet.Find(id);
        }


        public DbContext GetContext()
        {
            return this.context;
        }

        public DbSet<T> GetDbSet()
        {
            return this.dbSet;
        }


        public void Update(T entity, object key, Func<T, bool> predicate)
        {
            try
            {
                lock (this.Locker)
                {
                    if (entity != null)
                    {
                        if (predicate != null && this.context.Set<T>().Local.Any(predicate))
                        {
                        this.context.Entry(entity).State = EntityState.Modified;
                        }
                        else
                        {
                            T existing = this.context.Set<T>().FindAsync(key).Result;
                            if (existing != null)
                            {
                                //remember ef only set value for scalar property no related entity property
                                this.context.Entry(existing).CurrentValues.SetValues(entity);
                            }
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        public void Update(object entity, object key, Type type)
        {
            lock (this.Locker)
            {
                if (entity != null)
                {
                    DbSet dbSet = this.context.Set(type);
                    object existing = dbSet.FindAsync(key).Result;
                    if (existing != null)
                    {
                        //remember ef only set value for scalar property no related entity property
                        this.context.Entry(existing).CurrentValues.SetValues(entity);
                        this.context.SaveChanges();
                    }
                }
            }
        }
    }
}
