﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CommonLib.EFRepositories.Interface;
using System.Data.Entity;
using Newtonsoft.Json;
using Kendo.Mvc.Extensions;

namespace Pci.CommonLib.EFRepositories.EntityFramework.Repository
{
    public class RepositoryTyped : IRepositoryTyped
    {
        private readonly DbContext context;

        /// <summary>
        /// Use when need locking on multithreading
        /// </summary>
        protected readonly object locker = new object();

        public RepositoryTyped(IContextProvider provider)
        {
            this.context = provider.GetContext();
        }

        public string GetDetail(string type, object[] ids)
        {
            var result = this.context.Set(Type.GetType(type)).Find(ids);
            return JsonConvert.SerializeObject(result, Formatting.None,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
        }


        public void Update(object entity, object key, Type type)
        {
            lock(this.locker)
            {
                if(entity != null)
                {
                    DbSet dbSet = this.context.Set(type);
                    object existing = dbSet.FindAsync(key).Result;
                    if (existing != null)
                    {
                        //remember ef only set value for scalar property no related entity property
                        this.context.Entry(existing).CurrentValues.SetValues(entity);
                    }
                }
            }
        }


        public Kendo.Mvc.UI.DataSourceResult GetPageData(string type, Kendo.Mvc.UI.DataSourceRequest request)
        {
            return this.context.Set(Type.GetType(type)).ToDataSourceResult(request);
        }

        public DbContext GetContext()
        {
            return this.context;
        }
    }
}
