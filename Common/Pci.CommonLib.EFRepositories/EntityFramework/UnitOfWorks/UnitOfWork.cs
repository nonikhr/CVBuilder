﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Validation;
using System.Data.Common;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CommonLib.EFRepositories.Interface;

namespace Pci.CommonLib.EFRepositories.EntityFramework.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        private DbContext dataContext;
        private bool Disposed;
        private ObjectContext objectCtx;
        private DbTransaction transaction;
        private ILogger logger;
        private bool isThrowException;

        public UnitOfWork(IContextProvider provider, ILogger logger)
        {
            this.dataContext = provider.GetContext();
            this.logger = logger;
            this.logger.Init(this.GetType().FullName);
        }

        // NOTE: Leave out the finalizer altogether if this class doesn't 
        // own unmanaged resources itself, but leave the other methods
        // exactly as they are. 
        ~UnitOfWork()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (!this.Disposed)
            {
                if (disposing)
                {
                    // free other managed objects that implement
                    // IDisposable only

                    try
                    {
                        if (this.dataContext != null)
                        {
                            this.dataContext.Dispose();
                            this.dataContext = null;
                        }
                    }
                    catch (ObjectDisposedException e)
                    {
                        this.logger.SetRethrowOnError(isThrowException);
                        logger.LogOnError("Unit Of Work Disposed", e);
                        //the objectContext has already been disposed
                    }
                }

                this.Disposed = true;
            }
        }
        public int SaveChanges()
        {
            int retVal = 0;
            try
            {
                retVal = this.dataContext.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                StringBuilder sb = new StringBuilder();
                dbEx.EntityValidationErrors.ToList().ForEach((e) =>
                {
                    if (sb.Length > 0)
                    {
                        sb.Append("; ");
                    }

                    sb.Append(e.ValidationErrors);
                });
                this.logger.SetRethrowOnError(isThrowException);
                this.logger.LogOnError("Save To Database", new Exception(sb.ToString()));

                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string stringMessage = string.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                        this.logger.LogOnError("Save To Database", new Exception(stringMessage.ToString()));

                    }
                }
                throw raise;

            }
            catch (Exception e)
            {
                this.logger.SetRethrowOnError(isThrowException);
                this.logger.LogOnError("Save To Database", e);
            }

            return retVal;
        }

        public bool Commit()
        {
            try
            {
                this.transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                this.logger.SetRethrowOnError(isThrowException);
                this.logger.LogOnError("Transaction Commit", e);
                this.transaction.Rollback();
                return false;
            }
        }

        public void Rollback()
        {
            this.transaction.Rollback();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public void SetThrowExceptionOnError(bool isThrowException)
        {
            this.isThrowException = isThrowException;
            this.logger.SetRethrowOnError(isThrowException);
        }
    }
}
