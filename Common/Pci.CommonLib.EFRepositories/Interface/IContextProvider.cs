﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Pci.CommonLib.EFRepositories.Interface
{
    public interface IContextProvider
    {
        DbContext Initiate();
        DbContext GetContext();
    }
}
