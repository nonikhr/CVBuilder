﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Pci.CommonLib.Infrastructure.Model;

namespace Pci.CommonLib.EFRepositories.Interface
{
    public interface IRepository<T> where T : class
    {

        [Logged]
        void Create(T entity);

        [Logged]
        void Delete(T entity);

        [Logged]
        void Delete(object id);

        [Logged]
        void Update(T entity, object key, Func<T, bool> predicate);

        [Logged]
        void Update(object entity, object key, Type type);

        [Logged]
        List<T> GetList(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "");

        [Logged]
        T GetById(object id);

        DbContext GetContext();
        DbSet<T> GetDbSet();
    }
}
