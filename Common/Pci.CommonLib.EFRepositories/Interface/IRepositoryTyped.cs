﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using System.Data.Entity;
using Pci.CommonLib.Infrastructure.Model;


namespace Pci.CommonLib.EFRepositories.Interface
{
    public interface IRepositoryTyped
    {
        [Logged]
        string GetDetail(string type, object[] ids);

        [Logged]
        void Update(object entity, object key, Type type);

        [Logged]
        DataSourceResult GetPageData(string type, Kendo.Mvc.UI.DataSourceRequest request);

        DbContext GetContext();
    }
}
