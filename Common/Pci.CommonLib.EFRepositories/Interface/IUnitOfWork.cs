﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CommonLib.Infrastructure.Model;

namespace Pci.CommonLib.EFRepositories.Interface
{
    public interface IUnitOfWork : IDisposable
    {
        [Logged]
        int SaveChanges();

        [Logged]
        bool Commit();

        [Logged]
        void Rollback();

        [Logged]
        void SetThrowExceptionOnError(bool isThrowException);
    }
}
