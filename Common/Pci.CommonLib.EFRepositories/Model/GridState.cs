﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pci.CommonLib.EFRepositories.Model
{
    public class GridState
    {
        public string Aggregate { get; set; }
        public string Filter { get; set; }
        public string Sort { get; set; }
        public string Group { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}
