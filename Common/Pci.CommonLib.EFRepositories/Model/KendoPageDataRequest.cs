﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pci.CommonLib.EFRepositories.Model
{
    public class KendoPageDataRequest
    {
        public IList<Kendo.Mvc.AggregateDescriptor> Aggregates { get; set; }
        public List<string> Filters { get; set; }
        public List<string> FilterType { get; set; }

        public IList<Kendo.Mvc.GroupDescriptor> Groups { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public IList<Kendo.Mvc.SortDescriptor> Sorts { get; set; }
        public String FilterString { get; set; }
    }
}
