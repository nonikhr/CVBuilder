﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pci.CommonLib.EFRepositories.Model
{
    public class KendoPageDataResult<T> where T : class
    {
        public IEnumerable<Kendo.Mvc.Infrastructure.AggregateResult> AggregateResults { get; set; }
        public IEnumerable<T> Data { get; set; }
        public object Error { get; set; }
        public int Total { get; set; }
    }
}
