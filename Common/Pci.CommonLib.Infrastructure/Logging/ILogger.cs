﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pci.CommonLib.Infrastructure.Logging
{
    public interface ILogger
    {
        /// <summary>
        /// Initializes the specified Logger by its name.
        /// </summary>
        /// <param name="typeName">Name of the Logger.</param>
        void Init(string typeName);
        /// <summary>
        /// Logs the on begin of action.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="args">The arguments.</param>
        void LogOnBegin(string methodName, object[] args);
        /// <summary>
        /// Logs the on completed of action.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="args">The arguments.</param>
        /// <param name="result">The result.</param>
        void LogOnCompleted(string methodName, object[] args, object result);
        /// <summary>
        /// Any action required to proceed when error occured.
        /// example save detail error into log and rethrow new error with humanized friendly message
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="args">The arguments.</param>
        /// <param name="e">The exception.</param>
        /// <returns></returns>
        Exception ErrorHandler(string methodName, object[] args, Exception e);
        /// <summary>
        /// Logs the on error.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="e">The exception.</param>
        void LogOnError(string message, Exception e);
        /// <summary>
        /// Logs the message.
        /// </summary>
        /// <param name="message">The message.</param>
        void LogMessage(string message);

        /// <summary>
        /// Sets the rethrow on error.
        /// </summary>
        /// <param name="isRethrow">if set to <c>true</c> [is rethrow].</param>
        void SetRethrowOnError(bool isRethrow);
    }
}
