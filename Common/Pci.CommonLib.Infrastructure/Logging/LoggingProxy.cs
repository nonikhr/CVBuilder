﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Proxies;
using System.Reflection;
using Pci.CommonLib.Infrastructure.Model;

namespace Pci.CommonLib.Infrastructure.Logging
{
    public class LoggingProxy<T> : RealProxy
    {
        private readonly T Decorated;
        private readonly ILogger Logger;

        public LoggingProxy(T decorated, ILogger logger)
            : base(typeof(T))
        {
            this.Decorated = decorated;
            this.Logger = logger;
        }

        /// <summary>
        /// Invoke
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public override IMessage Invoke(IMessage msg)
        {
            var methodCall = msg as IMethodCallMessage;
            var methodInfo = methodCall.MethodBase as MethodInfo;
            var isLogged = (Logged)methodInfo.GetCustomAttributes(typeof(Logged)).FirstOrDefault() != null;

            Logger.Init(Decorated.GetType().FullName);

            if(isLogged)
            {
                Logger.LogOnBegin(methodCall.MethodName, methodCall.InArgs);
            }

            try
            {
                var result = methodInfo.Invoke(this.Decorated, methodCall.InArgs);

                if(isLogged)
                {
                    Logger.LogOnCompleted(methodCall.MethodName, methodCall.InArgs, result);
                }

                return new ReturnMessage(result, null, 0, methodCall.LogicalCallContext, methodCall);
            }
            catch(Exception e)
            {
                Exception returnException = Logger.ErrorHandler(methodCall.MethodName, methodCall.InArgs, e);
                return new ReturnMessage(returnException, methodCall);
            }
        }
    }
}
