﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

namespace Pci.CvBuilder.Entity
{
    using Pci.CvBuilder.Utility.Encryption;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
   
        public partial class CvBuilderEntities : DbContext
        {
            public CvBuilderEntities()
                : base(ReadConfig("CvBuilderEntities"))
            {
            }
            private static string ReadConfig(string connStringName)
            {
                return ConfigFile.ReadEfConnectionString(connStringName);
            }

           
        }
    }

