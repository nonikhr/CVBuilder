﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Pci.CommonLib.EFRepositories.Interface;

namespace Pci.CvBuilder.Entity.Provider
{
    public class ContextProvider : IContextProvider
    {
        private DbContext context;

        public System.Data.Entity.DbContext GetContext()
        {
            if (context == null)
            {
                InitiateContextSpeed();
                context = new CvBuilderEntities();
            }
            return context;
        }

        public DbContext Initiate()
        {
            InitiateContextSpeed();
            return context;
        }

        /// <summary>
        /// Initiates the context for speed transaction.
        /// </summary>
        private void InitiateContextSpeed()
        {
            this.context = new CvBuilderEntities();
            this.context.Configuration.AutoDetectChangesEnabled = false;
            this.context.Configuration.ValidateOnSaveEnabled = false;
        }

    }
}
