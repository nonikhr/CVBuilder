﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using log4net;
using Newtonsoft.Json;
using Pci.CommonLib.Infrastructure.Logging;

namespace Pci.CvBuilder.Infrastructure.Common
{
    /// <summary>
    /// Logging implementation using Log4net
    /// </summary>
    public class Logging : ILogger
    {
        /// <summary>
        /// The logger cache to reduce cost in inititate log4net LogManager
        /// </summary>
        private static ConcurrentDictionary<string, ILog> LoggerCache = new ConcurrentDictionary<string, ILog>();

        private bool isRethrow;

        /// <summary>
        /// The log4net logger
        /// </summary>
        private ILog Logger;

        /// <summary>
        /// Initialize the specified type name.
        /// </summary>
        /// <param name="typeName"></param>
        public void Init(string typeName)
        {
            LoggerCache.AddOrUpdate(typeName, LogManager.GetLogger(typeName), (x, y) => y);
            this.Logger = LoggerCache[typeName];
            this.isRethrow = false;
        }

        public void LogOnBegin(string methodName, object[] args)
        {
            string logMessage = string.Format("{0}", methodName);

            if (this.Logger.IsDebugEnabled) // Log level deubg then include parameter : see log4net level
            {
                var paramArgs = string.Join(", ", JsonConvert.SerializeObject(args, Formatting.None,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    })
                    );
                this.Logger.Debug(string.Format("Begin --> {0}({1})", logMessage, paramArgs));
            }
            else if(this.Logger.IsInfoEnabled) // log level info
            {
                this.Logger.Info(string.Format("Begin --> {0}", logMessage));
            }
        }

        /// <summary>
        /// Logs the on completed
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="args"></param>
        /// <param name="result"></param>
        public void LogOnCompleted(string methodName, object[] args, object result)
        {
            if(this.Logger.IsDebugEnabled) //Log level debug then include result
            {
                if(result == null)
                {
                    result = "void/null";
                }

                this.Logger.Debug(string.Format("Complete --> {0} Result: {1}", methodName,
                    JsonConvert.SerializeObject(result, Formatting.None, new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    }))
                    );
            }
            else if(this.Logger.IsInfoEnabled) // Log level info
            {
                this.Logger.Info(string.Format("Complete --> {0}", methodName));
            }
        }

        /// <summary>
        /// Errors the handler.
        /// </summary>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="args">The arguments.</param>
        /// <param name="e">The exception.</param>
        /// <returns></returns>
        /// <exception cref="System.ApplicationException">always rethrow application exception and user friendly message</exception>
        public Exception ErrorHandler(string methodName, object[] args, Exception e)
        {
            if (this.Logger.IsDebugEnabled)
            {
                var paramArgs = string.Join(", ", JsonConvert.SerializeObject(args, Formatting.None,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    })
                );
                this.Logger.Error(string.Format("Error --> {0}({1}) ", methodName, paramArgs), e);
            }
            else
            {
                this.Logger.Error(string.Format("Error --> {0} ", methodName), e);
            }

            if (e.GetType() == typeof(ApplicationException))
            {
                return e;
            }
            else
            {
                throw new ApplicationException(Resources.GenericError); // rethrow user friendly message
            }
        }

        /// <summary>
        /// Logs the on error.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="e"></param>
        public void LogOnError(string message, Exception e)
        {
            this.Logger.Error(string.Format("Error --> {0} ", message), e);
            if (this.isRethrow)
            {
                if (e.GetType() == typeof(ApplicationException))
                {
                    throw e;
                }
                else
                {
                    throw new ApplicationException(Resources.GenericError); // rethrow user friendly message
                }
            }
        }

        public void LogMessage(string message)
        {
            if (this.Logger.IsDebugEnabled)
            {
                this.Logger.Debug(message);
            }
            else if (this.Logger.IsInfoEnabled)
            {
                this.Logger.Info(message);
            }
        }

        public void SetRethrowOnError(bool isRethrow)
        {
            this.isRethrow = isRethrow;
        }
      
    }
}
