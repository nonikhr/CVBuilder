﻿using System.Runtime.Serialization;

namespace Pci.CvBuilder.Message
{
    [DataContract]
    public class BaseRequest
    {
        [DataMember]
        public string UserName;
    }
}
