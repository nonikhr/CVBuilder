﻿using System.Runtime.Serialization;

namespace Pci.CvBuilder.Message
{
    [DataContract]
    public class DetailRequest : BaseRequest
    {
        [DataMember]
        public string Type;
        [DataMember]
        public long Id;
    }
}
