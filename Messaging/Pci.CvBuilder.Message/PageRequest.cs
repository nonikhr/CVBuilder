﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Pci.CommonLib.EFRepositories.Model;

namespace Pci.CvBuilder.Message
{
    [DataContract]
    public class PageRequest : BaseRequest
    {
        [DataMember]
        public string Type;

        [DataMember]
        public GridState Request;
    }
}
