﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CommonLib.EFRepositories.Interface;

namespace Pci.CvBuilder.ApplicationRepository.Common
{
    public class RepositoryFactory
    {
        private static ILogger logger;

        // Rahasia
        // Test connect direct push
        //
        //

        public static IRepository<T> Create<T>(IRepository<T> repository, ILogger log)
            where T : class
        {
            logger = log;
            var repoProxy = (IRepository<T>)new LoggingProxy<IRepository<T>>(repository, log).GetTransparentProxy();
            return repoProxy;
        }


    }
}
