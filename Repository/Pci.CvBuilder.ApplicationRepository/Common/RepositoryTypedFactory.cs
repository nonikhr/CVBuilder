﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CommonLib.EFRepositories.Interface;

namespace Pci.CvBuilder.ApplicationRepository.Common
{
    public class RepositoryTypedFactory
    {
        private static ILogger logger;

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        public static IRepositoryTyped Create(IRepositoryTyped repository, ILogger log)
        {
            logger = log;
            var repoProxy = (IRepositoryTyped)new LoggingProxy<IRepositoryTyped>(repository, log).GetTransparentProxy();
            return repoProxy;
        }
    }
}
