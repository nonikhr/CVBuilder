﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.EntityFramework.Repository;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.Infrastructure.Logging;

namespace Pci.CvBuilder.ApplicationRepository.ConfigurationParameterRepo
{
    public class ConfigurationParameterRepository : IConfigurationParameterRepository
    {
        private readonly IRepository<Config> repository;
        private readonly IUnitOfWork uow;

        public ConfigurationParameterRepository(ILogger logger,
            Repository<Config> repository,
            IUnitOfWork uow)
        {
            this.repository = repository;
            this.uow = uow;
        }

        /// <summary>
        /// Get parameter configuration
        /// </summary>
        /// <returns></returns>
        public Config GetParameterConfiguration()
        {
            return (Config)this.repository.GetDbSet().FirstOrDefault();
        }

        /// <summary>
        /// Update Config
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public int Update(Config config)
        {
            this.repository.Update(config, config.Id, null);
            return this.uow.SaveChanges();
        }
    }
}
