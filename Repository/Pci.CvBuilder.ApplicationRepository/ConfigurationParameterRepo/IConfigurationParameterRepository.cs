﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.ApplicationRepository.ConfigurationParameterRepo
{
    public interface IConfigurationParameterRepository
    {
        Config GetParameterConfiguration();
        int Update(Config config);
        //List<Config> GetParameterConfigurationPerRow();
    }
}
