﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.EFRepositories.EntityFramework.Repository;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationRepository.DocumentRepo
{
    public class DocumentRepository : IDocumentRepository
    {
        private readonly IRepository<Document> repository;
        private readonly IUnitOfWork uow;

        public DocumentRepository(ILogger logger, Repository<Document> repository,
            IUnitOfWork uow)
        {
            this.repository = RepositoryFactory.Create<Document>(repository, logger);
            this.uow = uow;
        }

        public void Create(Document document)
        {
            this.repository.Create(document);
            this.uow.SaveChanges();
        }

        public void Update(Document document)
        {
            this.repository.Update(document, document.Id, null);
            this.uow.SaveChanges();
        }

        public int Delete(Document document)
        {
            this.repository.Delete(document.Id);
            return this.uow.SaveChanges();
        }

        public List<Document> GetDocumentList()
        {
            return this.repository.GetDbSet().ToList();
        }

        public Document GetDocumentById(int id)
        {
            Document document = new Document();
            document = this.repository.GetDbSet().Where(o => o.Id == id).FirstOrDefault();
            return document;
        }
    }
}
