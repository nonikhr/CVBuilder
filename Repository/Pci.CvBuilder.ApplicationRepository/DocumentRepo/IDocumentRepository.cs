﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.ApplicationRepository.DocumentRepo
{
    public interface IDocumentRepository
    {
        void Create(Document document);
        void Update(Document document);
        int Delete(Document document);
        List<Document> GetDocumentList();
        Document GetDocumentById(int id);
    }
}
