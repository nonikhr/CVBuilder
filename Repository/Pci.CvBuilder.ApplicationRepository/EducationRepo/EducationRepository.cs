﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.EFRepositories.EntityFramework.Repository;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationRepository.EducationRepo
{
    public class EducationRepository : IEducationRepository
    {
        private readonly IRepository<Education> repository;
        private readonly IUnitOfWork uow;

        public EducationRepository(ILogger logger, Repository<Education> repository,
            IUnitOfWork uow)
        {
            this.repository = RepositoryFactory.Create<Education>(repository, logger);
            this.uow = uow;
        }

        /// <summary>
        /// Create Education
        /// </summary>
        /// <param name="education"></param>
        public void Create(Education education)
        {
            this.repository.Create(education);
            this.uow.SaveChanges();
        }

        /// <summary>
        /// Update Education
        /// </summary>
        /// <param name="education"></param>
        public void Update(Education education)
        {
            this.repository.Update(education, education.Id, null);
            this.uow.SaveChanges();
        }

        /// <summary>
        /// Delete Education
        /// </summary>
        /// <param name="education"></param>
        /// <returns></returns>
        public int Delete(Education education)
        {
                this.repository.Delete(education.Id);
                return this.uow.SaveChanges();
        }

        /// <summary>
        /// Get Education by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Education GetUserEducationById(int id)
        {
                Education education = new Education();
                education = this.repository.GetDbSet().Where(o => o.Id == id).FirstOrDefault();
                return education;
        }

        /// <summary>
        /// Get List Education
        /// </summary>
        /// <returns></returns>
        public List<Education> GetUserEducationList()
        {
            return this.repository.GetDbSet().ToList();
        }
    }
}