﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.ApplicationRepository.EducationRepo
{
    public interface IEducationRepository
    {
        void Create(Education education);
        void Update(Education education);
        int Delete(Education education);
        Education GetUserEducationById(int id);
        List<Education> GetUserEducationList();
    }
}
