﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.EFRepositories.EntityFramework.Repository;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationRepository.ExperienceRepo
{
    public class ExperienceRepository : IExperienceRepository
    {
        private readonly IRepository<Experience> repository;
        private readonly IUnitOfWork uow;

        public ExperienceRepository(ILogger logger, Repository<Experience> repository,
        IUnitOfWork uow)
        {
            this.repository = RepositoryFactory.Create<Experience>(repository, logger);
            this.uow = uow;
        }

        /// <summary>
        /// Create Experience
        /// </summary>
        /// <param name="education"></param>
        public void Create(Experience experience)
        {
            this.repository.Create(experience);
            this.uow.SaveChanges();
        }

        /// <summary>
        /// Update Experience
        /// </summary>
        /// <param name="education"></param>
        public void Update(Experience experience)
        {
            this.repository.Update(experience, experience.Id, null);
            this.uow.SaveChanges();
        }

        /// <summary>
        /// Delete Education
        /// </summary>
        /// <param name="education"></param>
        /// <returns></returns>
        public int Delete(Experience experience)
        {
            this.repository.Delete(experience.Id);
            return this.uow.SaveChanges();
        }

        /// <summary>
        /// Get Experience by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Experience GetExperienceById(int id)
        {
            Experience experience = new Experience();
            experience = this.repository.GetDbSet().Where(o => o.Id == id).FirstOrDefault();
            return experience;
        }

        /// <summary>
        /// Get List Experience
        /// </summary>
        /// <returns></returns>
        public List<Experience> GetExperienceList()
        {
            return this.repository.GetDbSet().ToList();
        }
    }
}
