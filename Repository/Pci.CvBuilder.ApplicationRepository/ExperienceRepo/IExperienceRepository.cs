﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.ApplicationRepository.ExperienceRepo
{
    public interface IExperienceRepository
    {
        void Create(Experience experience);
        void Update(Experience experience);
        int Delete(Experience experience);
        List<Experience> GetExperienceList();
        Experience GetExperienceById(int id);


    }
}
