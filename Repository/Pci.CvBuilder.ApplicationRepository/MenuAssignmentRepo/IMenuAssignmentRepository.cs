﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.ApplicationRepository.MenuAssignmentRepo
{
    public interface IMenuAssignmentRepository
    {
        List<MenuAssignment> GetMenuByUserGroupId(long userGroupId);
        //long GetMenuId(string urlClassName);
    }
}
