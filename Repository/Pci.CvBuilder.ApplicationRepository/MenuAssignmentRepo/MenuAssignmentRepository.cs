﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.EntityFramework.Repository;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.Infrastructure.Logging;

namespace Pci.CvBuilder.ApplicationRepository.MenuAssignmentRepo
{
    public class MenuAssignmentRepository : IMenuAssignmentRepository
    {
        private readonly IRepository<MenuAssignment> repository;
        private readonly IRepository<MenuAsssignmentApproval> approvalRepository;
        private readonly IRepository<Menu> menuRepository;

        public MenuAssignmentRepository(ILogger logger,
            IRepository<MenuAssignment> repository,
            IRepository<MenuAsssignmentApproval> approvalRepository,
            IRepository<Menu> repositoryMenu)
        {
            this.repository = repository;
            this.approvalRepository = approvalRepository;
            this.menuRepository = repositoryMenu;
        }

        /// <summary>
        /// Get list menu by user group id
        /// </summary>
        /// <param name="userGroupId"></param>
        /// <returns></returns>
        public List<MenuAssignment> GetMenuByUserGroupId(long userGroupId)
        {
            List<MenuAssignment> listMenuAssignment = new List<MenuAssignment>();
            
            try
            {
                var menuAssignmentApprovalId = this.approvalRepository.GetDbSet()
                    .Where(o => o.UserGroupId == userGroupId)
                    .Select(o => o.Id);

                if (menuAssignmentApprovalId.Count() > 0)
                {
                    listMenuAssignment = this.repository.GetDbSet()
                        .Where(o => o.MenuAssignmentApproval == menuAssignmentApprovalId.FirstOrDefault()).ToList();
                }
            }
            catch
            {
                throw;
            }

            return listMenuAssignment;
        }

        ///// <summary>
        ///// Get menu Id
        ///// </summary>
        ///// <param name="urlClassName"></param>
        ///// <returns></returns>
        //public long GetMenuId(string urlClassName)
        //{
        //    var menuId = this.menuRepository.GetDbSet()
        //        .Where(o => o.Url.Trim().Substring(4, urlClassName.Length).Replace("/", "") == urlClassName)
        //        .Select(o => o.Id)
        //        .FirstOrDefault();

        //    return menuId;
        //}
    }
}
