﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.EFRepositories.EntityFramework.Repository;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationRepository.PersonalInformationRepo
{
    public class PersonalInformationRepository : IPersonalInformationRepository
    {
        private readonly IRepository<User> repositoryUser;
        private readonly IUnitOfWork uow;

        public PersonalInformationRepository(ILogger logger, 
            Repository<User> repositoryUser,
            IUnitOfWork uow)
        {
            this.repositoryUser = RepositoryFactory.Create<User>(repositoryUser, logger);
            this.uow = uow;
        }

        /// <summary>
        /// Create Personal Information
        /// </summary>
        /// <param name="personalInformation"></param>
        //public void Create(PersonalInformation personalInformation)
        //{
        //    this.repository.Create(personalInformation);
        //    this.uow.SaveChanges();
        //}

        /// <summary>
        /// Update Personal Information
        /// </summary>
        /// <param name="personalInformation"></param>
        public void Update(User user)
        {
            this.repositoryUser.Update(user, user.Id, null);
            this.uow.SaveChanges();
        }

        /// <summary>
        /// Get Id Personal Information
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //public PersonalInformation GetPersonalInformationById(int id)
        //{
        //    PersonalInformation personalInformation = new PersonalInformation();
        //    personalInformation = this.repository.GetDbSet().Where(o => o.Id == id).FirstOrDefault();
        //    return personalInformation;
        //}
    }
}
