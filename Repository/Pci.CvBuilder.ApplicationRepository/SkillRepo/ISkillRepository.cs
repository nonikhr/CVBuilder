﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.ApplicationRepository.SkillRepo
{
    public interface ISkillRepository
    {
        void Create(Skill skill);
        void Update(Skill skill);
        int Delete(Skill skill);
        List<Skill> GetSkillList();
        Skill GetSkillById(int id);
    }
}
