﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.EFRepositories.EntityFramework.Repository;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationRepository.SkillRepo
{
    public class SkillRepository : ISkillRepository
    {
        private readonly IRepository<Skill> repository;
        private readonly IUnitOfWork uow;

        public SkillRepository(ILogger logger, Repository<Skill> repository,
            IUnitOfWork uow)
        {
            this.repository = RepositoryFactory.Create<Skill>(repository, logger);
            this.uow = uow;
        }

        /// <summary>
        /// Create Skill
        /// </summary>
        /// <param name="skill"></param>
        public void Create(Skill skill)
        {
            this.repository.Create(skill);
            this.uow.SaveChanges();
        }

        /// <summary>
        /// Update Skill
        /// </summary>
        /// <param name="skill"></param>
        public void Update(Skill skill)
        {
            this.repository.Update(skill, skill.Id, null);
            this.uow.SaveChanges();
        }

        /// <summary>
        /// Delete Skill
        /// </summary>
        /// <param name="skill"></param>
        /// <returns></returns>
        public int Delete(Skill skill)
        {
            this.repository.Delete(skill.Id);
            return this.uow.SaveChanges();
        }

        /// <summary>
        /// Get List Skill
        /// </summary>
        /// <returns></returns>
        public List<Skill> GetSkillList()
        {
            return this.repository.GetDbSet().ToList();
        }

        /// <summary>
        /// Get Id Skill
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Skill GetSkillById(int id)
        {
            Skill skill = new Skill();
            skill = this.repository.GetDbSet().Where(o => o.Id == id).FirstOrDefault();
            return skill;
        }
    }
}
