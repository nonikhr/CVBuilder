﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.ApplicationRepository.TrainingRepo
{
    public interface ITrainingRepository
    {
        void Create(Training training);
        void Update(Training training);
        int Delete(Training training);
        List<Training> GetTrainingList();
        Training GetTrainingById(int id);
    }
}
