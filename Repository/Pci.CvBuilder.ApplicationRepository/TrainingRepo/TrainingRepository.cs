﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.EFRepositories.EntityFramework.Repository;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationRepository.TrainingRepo
{
    public class TrainingRepository : ITrainingRepository
    {
        private readonly IRepository<Training> repository;
        private readonly IUnitOfWork uow;

        public TrainingRepository(ILogger logger, Repository<Training> repository,
            IUnitOfWork uow)
        {
            this.repository = RepositoryFactory.Create<Training>(repository, logger);
            this.uow = uow;
        }


        /// <summary>
        /// Create training
        /// </summary>
        /// <param name="training"></param>
        public void Create(Training training)
        {
            this.repository.Create(training);
            this.uow.SaveChanges();
        }

        /// <summary>
        /// Update Training
        /// </summary>
        /// <param name="training"></param>
        public void Update(Training training)
        {
            this.repository.Update(training, training.Id, null);
            this.uow.SaveChanges();
        }

        /// <summary>
        /// Delete Training
        /// </summary>
        /// <param name="training"></param>
        /// <returns></returns>
        public int Delete(Training training)
        {
            this.repository.Delete(training.Id);
            return this.uow.SaveChanges();
        }

        /// <summary>
        /// Get Training by ID
        /// </summary>
        /// <returns></returns>
        public List<Training> GetTrainingList()
        {
            return this.repository.GetDbSet().ToList();
        }

        /// <summary>
        /// Get List Training
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Training GetTrainingById(int id)
        {
            Training training = new Training();
            training = this.repository.GetDbSet().Where(o => o.Id == id).FirstOrDefault();
            return training;
        }
    }
}
