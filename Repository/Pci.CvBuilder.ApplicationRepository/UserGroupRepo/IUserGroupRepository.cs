﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.ApplicationRepository.UserGroupRepo
{
    public interface IUserGroupRepository
    {
        void Create(UserGroup userGroup);
        void Update(UserGroup userGroup);
        List<UserGroup> GetUserGroupList();
        UserGroup GetUserGroupById(int id);
        int Delete(UserGroup userGroup);
    }
}
