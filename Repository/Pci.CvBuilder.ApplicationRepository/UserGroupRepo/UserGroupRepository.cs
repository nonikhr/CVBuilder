﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationRepository.UserGroupRepo
{
    public class UserGroupRepository : IUserGroupRepository
    {
        private readonly IRepository<UserGroup> repository;
        private readonly IUnitOfWork uow;

        public UserGroupRepository(ILogger logger,
            IRepository<UserGroup> repository,
            IUnitOfWork uow)
        {
            this.repository = RepositoryFactory.Create<UserGroup>(repository, logger);
            this.uow = uow;
        }

        /// <summary>
        /// Create User Group
        /// </summary>
        /// <param name="userGroup"></param>
        public void Create(UserGroup userGroup)
        {
            try
            {
                userGroup.Code = userGroup.Code.ToLower();
                if (this.repository.GetDbSet().Where(o => o.Code == userGroup.Code).Count() > 0)
                {
                    throw new ApplicationException("User Group already exist.");
                }
                else
                {
                    this.repository.Create(userGroup);
                    this.uow.SaveChanges();
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update User Group 
        /// </summary>
        /// <param name="userGroup"></param>
        public void Update(UserGroup userGroup)
        {
            try
            {
                userGroup.Code = userGroup.Code.ToLower();
                if (this.repository.GetDbSet().Where(o => o.Code == userGroup.Code && o.Id != userGroup.Id).Count() > 0)
                {
                    throw new ApplicationException("User Group already exist.");
                }
                else
                {
                    this.repository.Update(userGroup, userGroup.Id, null);
                    this.uow.SaveChanges();
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get UserGroup list
        /// </summary>
        /// <returns></returns>
        public List<UserGroup> GetUserGroupList()
        {
            return this.repository.GetDbSet().ToList();
        }

        /// <summary>
        /// Get User group by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserGroup GetUserGroupById(int id)
        {
            try
            {
                UserGroup userGroup = new UserGroup();
                userGroup = this.repository.GetDbSet().Where(o => o.Id == id).FirstOrDefault();
                return userGroup;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete user Group
        /// </summary>
        /// <param name="userGroup"></param>
        /// <returns></returns>
        public int Delete(UserGroup userGroup)
        {
            try
            {
                this.repository.Delete(userGroup.Id);
                return this.uow.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
    }
}
