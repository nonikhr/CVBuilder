﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.ApplicationRepository.UserRepo
{
    public interface IUserRepository
    {
        int Create(User user);
        int Update(User user);
        int Delete(User user);
        User GetUserById(int id);
        User GetUserByUserIdForDuplicate(string userId);
        User GetUserByUserId(string userId, string password);
        List<User> GetUserList();
    }
}
