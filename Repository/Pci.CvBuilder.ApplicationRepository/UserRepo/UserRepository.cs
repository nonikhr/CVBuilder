﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Pci.CvBuilder.Entity;
using Pci.CommonLib.EFRepositories.Interface;
using Pci.CommonLib.EFRepositories.EntityFramework.Repository;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.ApplicationRepository.Common;

namespace Pci.CvBuilder.ApplicationRepository.UserRepo
{
    public class UserRepository : IUserRepository
    {
        private readonly IRepository<User> repository;
        private readonly IUnitOfWork uow;

        public UserRepository(ILogger logger, Repository<User> repository,
            IUnitOfWork uow)
        {
            this.repository = RepositoryFactory.Create<User>(repository, logger);
            this.uow = uow;
        }
        public int Create(User user)
        {
            user.UserId = user.UserId.ToLower();
            if (this.repository.GetDbSet().Where(o => o.UserId == user.UserId).Count() > 0)
            {
                throw new ApplicationException("User Id already exists.");
            }
            else
            {
                this.repository.Create(user);
                return this.uow.SaveChanges();
            }
        }

        public int Update(User user)
        {
            user.UserId = user.UserId.ToLower();
            if (this.repository.GetDbSet().Where(o => o.UserId == user.UserId && o.Id != user.Id).Count() > 0)
            {
                throw new ApplicationException("User Id already exists.");
            }
            else
            {
                this.repository.Update(user, user.Id, null);
                return this.uow.SaveChanges();
            }
        }

        public int Delete(User user)
        {
            try
            {
                this.repository.Delete(user.Id);
                return this.uow.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get user by user Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User GetUserById(int id)
        {
            return (User)this.repository.GetDbSet().Where(o => o.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// Get user by user id for duplicate data
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public User GetUserByUserIdForDuplicate(string userId)
        {
            return (User)this.repository.GetDbSet().Where(o => o.UserId == userId.ToLower()).FirstOrDefault();
        }

        /// <summary>
        /// Get user by user id
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public User GetUserByUserId(string userId, string password)
        {
            try
            {
                User data = new User();
                data = this.repository.GetDbSet().Where(o => o.UserId == userId).FirstOrDefault();
                return data;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get list user
        /// </summary>
        /// <returns></returns>
        public List<User> GetUserList()
        {
            return this.repository.GetDbSet().ToList();
        }
    }
}
