﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.WcfServices
{
    [ServiceContract]
    public interface IUIConfigurationParameterServices
    {
        [OperationContract]
        string GetParameterConfiguration();

        [OperationContract]
        void UpdateConfig(Config config);
    }
}
