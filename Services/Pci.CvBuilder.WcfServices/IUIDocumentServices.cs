﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.WcfServices
{
    [ServiceContract]
    public interface IUIDocumentServices
    {
        [OperationContract]
        void Create(Document document);
        [OperationContract]
        void Update(Document document);
        [OperationContract]
        void Delete(Document document);
        [OperationContract]
        string GetDocumentList();
        [OperationContract]
        string GetDocumentById(int id);
    }
}
