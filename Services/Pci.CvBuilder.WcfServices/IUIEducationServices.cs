﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.WcfServices
{
    [ServiceContract]
    public interface IUIEducationServices
    {
        [OperationContract]
        void Create(Education education);
        [OperationContract]
        void Update(Education education);
        [OperationContract]
        void Delete(Education education);
        [OperationContract]
        string GetEducationList();
        [OperationContract]
        string GetEducationById(int id);
    }
}
