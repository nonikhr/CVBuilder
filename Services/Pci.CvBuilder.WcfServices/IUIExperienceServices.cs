﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.WcfServices
{
    [ServiceContract]
    public interface IUIExperienceServices
    {
        [OperationContract]
        void Create(Experience experience);
        [OperationContract]
        void Update(Experience experience);
        [OperationContract]
        void Delete(Experience experience);
        [OperationContract]
        string GetExperienceList();
        [OperationContract]
        string GetExperienceById(int id);
    }
}

