﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.WcfServices
{
    [ServiceContract]
    public interface IUIMenuAssignmentServices
    {
        /// <summary>
        /// Create menu assignment
        /// </summary>
        /// <param name="model"></param>
        [OperationContract]
        void CreateMenuAssignment(MenuAsssignmentApproval model);

        /// <summary>
        /// Update menu assignment
        /// </summary>
        /// <param name="model"></param>
        [OperationContract]
        int UpdateMenuAssignment(MenuAsssignmentApproval model);

        /// <summary>
        /// Delete menu assignment
        /// </summary>
        /// <param name="model"></param>
        [OperationContract]
        int DeleteMenuAssignment(MenuAsssignmentApproval model);

        /// <summary>
        /// menu access
        /// </summary>
        /// <param name="userGroupid"></param>
        /// <returns></returns>
        [OperationContract]
        string GetMenuByUserGroupId(long userGroupid);
        
    }
}
