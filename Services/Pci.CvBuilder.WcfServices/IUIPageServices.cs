﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Pci.CvBuilder.Message;

namespace Pci.CvBuilder.WcfServices
{
    [ServiceContract]
    public interface IUIPageServices
    {
        [OperationContract]
        string GetDetailData(DetailRequest request);

        [OperationContract]
        string GetPageData(PageRequest request);

        [OperationContract]
        string GetAllList(ListRequest request);

    }
}
