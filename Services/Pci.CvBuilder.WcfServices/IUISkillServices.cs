﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pci.CvBuilder.Entity;
using System.ServiceModel;

namespace Pci.CvBuilder.WcfServices
{
    [ServiceContract]
    public interface IUISkillServices
    {
        [OperationContract]
        void Create(Skill skill);
        [OperationContract]
        void Update(Skill skill);
        [OperationContract]
        void Delete(Skill skill);
        [OperationContract]
        string GetSkillList();
        [OperationContract]
        string GetSkillById(int id);
    }
}
