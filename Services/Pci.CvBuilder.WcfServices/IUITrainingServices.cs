﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.WcfServices
{
    [ServiceContract]
    public interface IUITrainingServices
    {
        [OperationContract]
        void Create(Training training);
        [OperationContract]
        void Update(Training training);
        [OperationContract]
        void Delete(Training training);
        [OperationContract]
        string GetTrainingById(int id);
        [OperationContract]
        string GetTrainingList();
    }
}
