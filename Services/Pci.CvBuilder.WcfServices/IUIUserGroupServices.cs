﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.WcfServices
{
    [ServiceContract]
    public interface IUIUserGroupServices
    {
        [OperationContract]
        void Create(UserGroup userGroup);

        [OperationContract]
        void Update(UserGroup userGroup);

        [OperationContract]
        void Delete(UserGroup userGroup);

        [OperationContract]
        string GetUserGroupList();

        [OperationContract]
        string GetUserGroupById(int id);
    }
}
