﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.WcfServices
{
    [ServiceContract]
    public interface IUIUserServices
    {
        /// <summary>
        /// Create User
        /// </summary>
        /// <param name="user"></param>
        [OperationContract]
        void CreateUser(User user);

        /// <summary>
        /// Update User
        /// </summary>
        /// <param name="user"></param>
        [OperationContract]
        void UpdateUser(User user);

        /// <summary>
        /// Update User logon
        /// </summary>
        /// <param name="user"></param>
        [OperationContract]
        void UpdateUserLogon(User user);

        /// <summary>
        /// Delete User
        /// </summary>
        /// <param name="user"></param>
        [OperationContract]
        void DeleteUser(User user);

        /// <summary>
        /// Get user by user id
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [OperationContract]
        string GetUserByUserId(string userId, string password);

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OperationContract]
        string GetUserById(int id);

        /// <summary>
        /// Get user list
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        string GetUserList();

    }
}
