﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pci.CvBuilder.UI.Models;
using Pci.CvBuilder.UI.Commons;
using Pci.CvBuilder.UI.UIServiceWorker.UserSW;

namespace Pci.CvBuilder.UI.Areas.Common.Controllers
{
    public class ChangesPasswordController : Controller
    {
        // GET: Common/ChangesPassword
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(ChangesPasswordModel model)
        {
            if(Crypto.Encryption(model.OldPassword) != Session["Password"].ToString())
            {
                ViewBag.Message = "The old password is invalid...";
                return View();
            }
            else if(Crypto.Encryption(model.NewPassword) != Crypto.Encryption(model.ConfirmPassword))
            {
                ViewBag.Message = "The password and confirmation password do not match.";
                return View();
            }
            else
            {
                Entity.User user = null;
                user = new UserServiceWorker().GetUserByUserId(this.Session["User"].ToString(), Crypto.Encryption(model.OldPassword));
                user.Password = Crypto.Encryption(model.NewPassword);

                //Update user password
                UpdateUserData(user);
                ViewBag.Message = "Changes Password is Successfully.";

                return View();
            }
        }

        public void UpdateUserData(Entity.User user)
        {
            try
            {
                Entity.User userUpdate = new Entity.User();

                userUpdate.Id = user.Id;
                userUpdate.UserId = user.UserId;
                userUpdate.UserName = user.UserName;
                userUpdate.Password = user.Password;
                userUpdate.UserGroup = user.UserGroup;

                new UserServiceWorker().UpdateUser(userUpdate);
            }
            catch
            {
                throw;
            }
        }
    }
}