﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Pci.CommonLib.EFRepositories.Model;
using Pci.CvBuilder.UI.UIServiceWorker.PageSW;
using Pci.CvBuilder.UI.UIServiceWorker.DocumentSW;
using Pci.CvBuilder.Entity;
using System.IO;

namespace Pci.CvBuilder.UI.Areas.Common.Controllers
{
    public class DocumentController : Controller
    {
        // GET: Common/Document
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Read data for gridview
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        {
            long id = Convert.ToInt32(this.Session["Id"]);
            var listData = new DocumentServiceWorker().GetDocumentList().Where(o=>o.UserId ==id).ToList();
            return Json(listData.ToDataSourceResult(request));
        }

        /// <summary>
        /// Detail Document
        /// </summary>
        /// <returns></returns>
        public ActionResult Detail()
        {
            Document document = new Document();
            return View(document);
        }

        /// <summary>
        /// Update document
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Update(int id)
        {
            Document document = new Document();
            document = new PageServiceWorker().GetDetailData<Document>(id);
            return View(document);
        }

        /// <summary>
        /// View Document
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult View(int id)
        {
            Document document = new Document();
            document = new PageServiceWorker().GetDetailData<Document>(id);
            return View(document);
        }

        /// <summary>
        /// Create Document
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Create(HttpPostedFileBase files, Document request)
        {
            try
            {
                if (files != null && files.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(files.FileName);

                    // check extension file
                    if (Path.GetExtension(fileName).ToLower() != ".pdf")
                    {
                        @ViewBag.Message = "File is not .PDF type.";
                    }
                    else
                    {
                        var physicalPath = Path.Combine(Server.MapPath("~/Content/Images/Document"), fileName);
                        files.SaveAs(physicalPath);

                        var userId = this.Session["Id"].ToString();

                        Document document = new Document();
                        document.UserId = int.Parse(userId);
                        document.DocumentType = request.DocumentType;
                        document.DocumentName = files.FileName;

                        new DocumentServiceWorker().CreateDocument(document);

                        @ViewBag.Message = "Success upload file";
                    }
                }
                else
                {
                    @ViewBag.Message = "Failed upload file";
                }
            }
            catch (Exception ex)
            {
                @ViewBag.Message = "ERROR:" + ex.Message.ToString();
            }
            return View("Index");
        }

        /// <summary>
        /// Update Document
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult UpdateData(HttpPostedFileBase files, Document request)
        {
            try
            {
                if (files != null && files.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(files.FileName);

                    // check extension file
                    if (Path.GetExtension(fileName).ToLower() != ".pdf")
                    {
                        @ViewBag.Message = "File is not .PDF type.";
                    }
                    else
                    {
                        var physicalPath = Path.Combine(Server.MapPath("~/Content/Images/Document"), fileName);
                        files.SaveAs(physicalPath);

                        var userId = this.Session["Id"].ToString();

                        Document document = new Document();
                        document.Id = request.Id;
                        document.UserId = int.Parse(userId);
                        document.DocumentType = request.DocumentType;
                        document.DocumentName = files.FileName;

                        new DocumentServiceWorker().UpdateDocument(document);
                        @ViewBag.Message = "Success upload file";
                    }
                }
                else
                {
                    @ViewBag.Message = "Failed upload file";
                }
            }
            catch (Exception ex)
            {
                @ViewBag.Message = "ERROR:" + ex.Message.ToString();
            }
            return View("Index");
        }

        public ActionResult Destroy(int id)
        {
            try
            {
                Document document = new Document();
                document = new PageServiceWorker().GetDetailData<Document>(id);

                new DocumentServiceWorker().DeleteDocument(document);
                return Json(new { success = true });
            }
            catch { throw; }
        }
    }
}