﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Pci.CommonLib.EFRepositories.Model;
using Pci.CvBuilder.UI.UIServiceWorker.PageSW;
using Pci.CvBuilder.UI.UIServiceWorker.EducationSW;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.UI.Areas.Common.Controllers
{
    public class EducationController : Controller
    {
        // GET: Common/Education
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Read data for gridview
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        {
            long id = Convert.ToInt32(this.Session["Id"]);
            var listData = new EducationServiceWorker().GetEducationList().Where(o => o.UserId == id).ToList();
            return Json(listData.ToDataSourceResult(request));
        }

        /// <summary>
        /// Detail 
        /// </summary>
        /// <returns></returns>
        public ActionResult Detail()
        {
            Education education = new Education();
            return View(education);
        }

        /// <summary>
        /// Update view
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Update(int id)
        {
            Education education = new Education();
            education = new PageServiceWorker().GetDetailData<Education>(id);
            return View(education);
        }

        /// <summary>
        /// View detail
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult View(int id)
        {
            Education education = new Education();
            education = new PageServiceWorker().GetDetailData<Education>(id);
            return View(education);
        }

        /// <summary>
        /// Create Education
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(Education request)
        {
            try
            {
                var userId = this.Session["Id"].ToString();

                Education education = new Education();
                education.UserId = int.Parse(userId);
                education.Institution = request.Institution;
                education.Degree = request.Degree;
                education.Major = request.Major;
                education.Focus = request.Focus;
                education.StartDate = request.StartDate;
                education.EndDate = request.EndDate;
                education.Grade = request.Grade;

                new EducationServiceWorker().CreateEducation(education);
                return Json(new { success = true });
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update Education
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateData(Education request)
        {
            try
            {
                var userId = this.Session["Id"].ToString();
                Education education = new Education();
                education.Id = request.Id;
                education.UserId = int.Parse(userId);
                education.Institution = request.Institution;
                education.Degree = request.Degree;
                education.Major = request.Major;
                education.Focus = request.Focus;
                education.StartDate = request.StartDate;
                education.EndDate = request.EndDate;
                education.Grade = request.Grade;

                new EducationServiceWorker().UpdateEducation(education);
                return Json(new { success = true });
            }
            catch
            {
                throw;
            }
        }

        //Test
        /// <summary>
        /// Delete data
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Destroy(int id)
        {
            try
            {
                Education education = new Education();
                education = new PageServiceWorker().GetDetailData<Education>(id);

                new EducationServiceWorker().DeleteEducation(education);
                return Json(new { success = true });
            }
            catch
            {
                throw;
            }
        }
    }
}