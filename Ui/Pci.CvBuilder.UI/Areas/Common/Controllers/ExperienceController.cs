﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Pci.CommonLib.EFRepositories.Model;
using Pci.CvBuilder.UI.UIServiceWorker.PageSW;
using Pci.CvBuilder.UI.UIServiceWorker.ExperienceSW;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.UI.Areas.Common.Controllers
{
    public class ExperienceController : Controller
    {
        // GET: Common/Experience
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Read data for gridview
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        {
            long id = Convert.ToInt32(this.Session["Id"]);
            var listData = new ExperienceServiceWorker().GetExperienceList().Where(o => o.UserId == id).ToList();
            return Json(listData.ToDataSourceResult(request));
        }

        /// <summary>
        /// Detail 
        /// </summary>
        /// <returns></returns>
        public ActionResult Detail()
        {
            Experience experience = new Experience();
            return View(experience);
        }

        /// <summary>
        /// Update view
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Update(int id)
        {
            Experience experience = new Experience();
            experience = new PageServiceWorker().GetDetailData<Experience>(id);
            return View(experience);
        }

        /// <summary>
        /// View detail
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult View(int id)
        {
            Experience experience = new Experience();
            experience = new PageServiceWorker().GetDetailData<Experience>(id);
            return View(experience);
        }

        /// <summary>
        /// Create Experience
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(Experience request)
        {
            try
            {
                var userId = this.Session["Id"].ToString();

                Experience experience = new Experience();
                experience.UserId = int.Parse(userId);
                experience.Company = request.Company;
                experience.JobTitle = request.JobTitle;
                experience.JobDescription = request.JobDescription;
                experience.StartDate = request.StartDate;
                experience.EndDate = request.EndDate;
                experience.StillWorking = request.StillWorking;
                //experience.StillWorking = request.StillWorking.Substring(0,1);
                experience.Salary = request.Salary;

                new ExperienceServiceWorker().CreateExperience(experience);
                return Json(new { success = true });
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update Experience
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateData(Experience request)
        {
            try
            {
                var userId = this.Session["Id"].ToString();
                Experience experience = new Experience();
                experience.Id = request.Id;
                experience.UserId = int.Parse(userId);
                experience.Company = request.Company;
                experience.JobTitle = request.JobTitle;
                experience.JobDescription = request.JobDescription;
                experience.StartDate = request.StartDate;
                experience.EndDate = request.EndDate;
                experience.StillWorking = request.StillWorking;
                //experience.StillWorking = request.StillWorking.Substring(0,1);
                experience.Salary = request.Salary;

                new ExperienceServiceWorker().UpdateExperience(experience);
                return Json(new { success = true });
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete data
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Destroy(int id)
        {
            try
            {
                Experience experience = new Experience();
                experience = new PageServiceWorker().GetDetailData<Experience>(id);

                new ExperienceServiceWorker().DeleteExperience(experience);
                return Json(new { success = true });
            }
            catch
            {
                throw;
            }
        }


    }
}