﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using NReco.PdfGenerator;
using System.Text;
using Pci.CvBuilder.UI.Areas.Common.Models;
using Pci.CvBuilder.UI.UIServiceWorker.UserSW;
using Pci.CvBuilder.Entity;
using Pci.CvBuilder.UI.UIServiceWorker.PageSW;
//using Aspose.Pdf;

namespace Pci.CvBuilder.UI.Areas.Common.Controllers
{
    public class GenerateCVController : Controller
    {
        // GET: Common/GenerateCV
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CvPage()
        {
            return View();
        }

        /// <summary>
        /// Render view to string
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public string RenderViewAsString(string viewName, object model)
        {
            long idUser = Convert.ToInt32(this.Session["Id"]);

            // Define model to render in html
            CvBase cvBase = GetCvBaseData(idUser);


            // create a string writer to recieve the HTML code
            StringWriter stringWriter = new StringWriter();

            // get the view to render
            ViewEngineResult viewResult = ViewEngines.Engines.FindView(ControllerContext,
                viewName, null);

            // create a context to render a view based on a model
            ViewContext viewContext = new ViewContext(
                ControllerContext,
                viewResult.View,
                new ViewDataDictionary(cvBase),
                new TempDataDictionary(),
                stringWriter
                );

            // render the view to a HTML code
            viewResult.View.Render(viewContext, stringWriter);

            // return the HTML code
            return stringWriter.ToString();


        }

        /// <summary>
        /// Generate CV
        /// </summary>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult GenerateCvToPdf()
        {
            var htmlToPdf = new HtmlToPdfConverter();

            string htmlContent = RenderViewAsString("CvPage", null);
            var pdfContentTye = "application/pdf";

            var result = File(htmlToPdf.GeneratePdf(htmlContent, null), pdfContentTye);
            return result;
        }

        /// <summary>
        /// Get CVBase data 
        /// </summary>
        /// <returns></returns>
        public CvBase GetCvBaseData(long idUser)
        {
            var data = new PageServiceWorker().GetAllList<User>()
                .Where(o => o.Id == idUser).FirstOrDefault();

            CvBase cvBase = new CvBase();
            cvBase.LogoImage = Server.MapPath("~/Content/Images/logo praweda.png");

            if (data != null)
            {
                cvBase.Name = data.Name;
                cvBase.Gender = data.Gender;
                cvBase.PlaceOfBirth = data.PlaceOfBirth;
                cvBase.DateOfBirth = Convert.ToDateTime(data.DateOfBirth).ToString("dd/MM/yyyy");
                cvBase.Address = data.Address;
                cvBase.MobilePhone = data.MobilePhone;
                cvBase.Email = data.Email;
                var pathImg = "~/Content/Images/ProfilePictures/" + data.Image;
                cvBase.Image = Server.MapPath(pathImg);
                cvBase.Education = this.EducationGetLoop(idUser);
                cvBase.Experience = this.ExperienceGetLoop(idUser);
                cvBase.Training = this.TrainingGetLoop(idUser);
                cvBase.Skill = this.SkillGetLoop(idUser);
            }

            return cvBase;
        }

        /// <summary>
        /// Education data loop
        /// </summary>
        /// <returns></returns>
        public string EducationGetLoop(long userId)
        {
            var listdata = new PageServiceWorker().GetAllList<Education>()
                .Where(o => o.UserId == userId).ToList();

            StringBuilder sb = new StringBuilder();

            if (listdata.Count > 0)
            {
                sb.Append("<br />");
                sb.Append("<h2 style='border-radius: 5px; background-color: lightgrey; width: 200%;'>Education</h2>");
                sb.Append("<table width='180%' style='border-bottom:1px solid #808080; border-top:1px solid #808080;'>");
                sb.Append("<tr style='text-align:left'>");
                sb.Append("<th>Institution</th>");
                sb.Append("<th>Degree</th>");
                sb.Append("<th>Major</th>");
                sb.Append("<th>Focus</th>");
                sb.Append("<th>Start Date</th>");
                sb.Append("<th>End Date</th>");
                sb.Append("<th>GPA</th>");
                sb.Append("</tr>");

                foreach (var data in listdata)
                {
                    sb.Append("<tr>");
                    sb.Append("<td width='300'>" + data.Institution + "</td>");
                    sb.Append("<td width='200'>" + data.Degree + "</td>");
                    sb.Append("<td width='200'>" + data.Major + "</td>");
                    sb.Append("<td width='200'>" + data.Focus + "</td>");
                    sb.Append("<td width='90'>" + Convert.ToDateTime(data.StartDate).ToString("dd/MM/yyyy") + "</td>");
                    sb.Append("<td width='90'>" + Convert.ToDateTime(data.EndDate).ToString("dd/MM/yyyy") + "</td>");
                    sb.Append("<td width='80'>" + data.Grade + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            else
                sb.Append("");

            return sb.ToString();
        }

        /// <summary>
        /// Experience data loop
        /// </summary>
        /// <returns></returns>
        public string ExperienceGetLoop(long userId)
        {
            var listdata = new PageServiceWorker().GetAllList<Experience>()
                .Where(o => o.UserId == userId).ToList();

            StringBuilder sb = new StringBuilder();

            if (listdata.Count > 0)
            {
                sb.Append("<br />");
                sb.Append("<h2 style='border-radius: 5px; background-color: lightgrey; width: 200%;'>Experience</h2>");
                sb.Append("<table width='180%' style='border-bottom:1px solid #808080; border-top:1px solid #808080;'>");
                sb.Append("<tr style='text-align:left'>");
                sb.Append("<th>Company</th>");
                sb.Append("<th>Job Title</th>");
                sb.Append("<th>Start Date</th>");
                sb.Append("<th>End Date</th>");
                sb.Append("<th>Still Working</th>");
                sb.Append("<th>Job Description</th>");
                sb.Append("</tr>");

                foreach (var data in listdata)
                {
                    sb.Append("<tr>");
                    sb.Append("<td width='150'>" + data.Company + "</td>");
                    sb.Append("<td width='150'>" + data.JobTitle + "</td>");
                    sb.Append("<td width='150'>" + Convert.ToDateTime(data.StartDate).ToString("dd/MM/yyyy") + "</td>");
                    sb.Append("<td width='150'>" + Convert.ToDateTime(data.EndDate).ToString("dd/MM/yyyy") + "</td>");
                    string stillWork = string.Empty;

                    if (data.StillWorking.ToLower() == "N")
                        stillWork = "No";
                    else
                        stillWork = "Yes";
                    sb.Append("<td width='150'>" + stillWork + "</td>");
                    sb.Append("<td width='150'>" + data.JobDescription + "</td>");
                    sb.Append("</tr>");
                }
            sb.Append("</table>");
            }
            
            else
                sb.Append("");

            return sb.ToString();
        }

        //public string ExperienceGetLoop(long userId)
        //{
        //    var listdata = new PageServiceWorker().GetAllList<Experience>()
        //        .Where(o => o.UserId == userId).ToList();

        //    StringBuilder sb = new StringBuilder();

        //    if (listdata.Count > 0)
        //    {
        //        sb.Append("<br />");
        //        sb.Append("<h2 style='border-radius: 5px; background-color: lightgrey; width: 200%;'>Experience</h2>");
        //        sb.Append("<table width='200%' style='border-top:1px solid #808080;'>");
        //        foreach (var data in listdata)
        //        {
        //            sb.Append("<tr>");
        //            sb.Append("<td width='300'>Company</td>");
        //            sb.Append("<td width='30'> : </td>");
        //            sb.Append("<td >" + data.Company + "</td>");
        //            sb.Append("</tr>");

        //            sb.Append("<tr>");
        //            sb.Append("<td width='300'>Job Title</td>");
        //            sb.Append("<td width='30'> : </td>");
        //            sb.Append("<td >" + data.JobTitle + "</td>");
        //            sb.Append("</tr>");

        //            sb.Append("<tr>");
        //            sb.Append("<td width='300'>Start Date</td>");
        //            sb.Append("<td width='30'> : </td>");
        //            sb.Append("<td>" + Convert.ToDateTime(data.StartDate).ToString("dd/MM/yyyy") + "</td>");
        //            sb.Append("</tr>");

        //            sb.Append("<tr>");
        //            sb.Append("<td width='300'>End Date</td>");
        //            sb.Append("<td width='30'> : </td>");
        //            sb.Append("<td >" + Convert.ToDateTime(data.EndDate).ToString("dd/MM/yyyy") + "</td>");
        //            sb.Append("</tr>");

        //            string stillWork = string.Empty;

        //            if (data.StillWorking.ToLower() == "N")
        //                stillWork = "No";
        //            else
        //                stillWork = "Yes";

        //            sb.Append("<tr>");
        //            sb.Append("<td width='300'>Still Working</td>");
        //            sb.Append("<td width='30'> : </td>");
        //            sb.Append("<td>" + stillWork + "</td>");
        //            sb.Append("</tr>");

        //            sb.Append("<tr>");
        //            sb.Append("<td width='300'>Job Description</td>");
        //            sb.Append("<td width='30'> : </td>");
        //            sb.Append("<td>" + data.JobDescription + "</td>");
        //            sb.Append("</tr>");

        //            sb.Append("<table width='200%' style='border-bottom:1px solid #808080;'>");
        //        }
        //        sb.Append("</table>");
        //    }
        //    else
        //        sb.Append("");

        //    return sb.ToString();
        //}

        /// <summary>
        /// Training data loop
        /// </summary>
        /// <returns></returns>
        public string TrainingGetLoop(long userId)
        {
            var listdata = new PageServiceWorker().GetAllList<Training>()
                .Where(o => o.UserId == userId).ToList();

            StringBuilder sb = new StringBuilder();

            if (listdata.Count > 0)
            {
                sb.Append("<br />");
                sb.Append("<h2 style='border-radius: 5px; background-color: lightgrey; width: 200%;'>Training</h2>");
                sb.Append("<table width='180%' style='border-bottom:1px solid #808080; border-top:1px solid #808080;'>");
                sb.Append("<tr style='text-align:left'>");
                sb.Append("<th>Program Name</th>");
                sb.Append("<th>Institution</th>");
                sb.Append("<th>Date</th>");
                sb.Append("<th>Certified</th>");
                sb.Append("</tr>");

                foreach (var data in listdata)
                {
                    sb.Append("<tr>");
                    sb.Append("<td width='300'>" + data.ProgramName + "</td>");
                    sb.Append("<td width='300'>" + data.Institution + "</td>");
                    sb.Append("<td width='100'>" + Convert.ToDateTime(data.Date).ToString("dd/MM/yyyy") + "</td>");

                    string sertifikat = string.Empty;
                    if (data.Certified == "Y")
                        sertifikat = "Yes";
                    else
                        sertifikat = "No";

                    sb.Append("<td width='100'>" + sertifikat + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            else
                sb.Append("");

            return sb.ToString();
        }

        /// <summary>
        /// Skill data loop
        /// </summary>
        /// <returns></returns>
        public string SkillGetLoop(long userId)
        {
            var listdata = new PageServiceWorker().GetAllList<Skill>()
                .Where(o => o.UserId == userId).ToList();

            StringBuilder sb = new StringBuilder();

            if (listdata.Count > 0)
            {
                sb.Append("<br />");
                sb.Append("<h2 style='border-radius: 5px; background-color: lightgrey; width: 200%;'>Skill</h2>");
                sb.Append("<table width='200%' style='border-bottom:1px solid #808080; border-top:1px solid #808080;'>");
                sb.Append("<tr style='text-align:left'>");
                sb.Append("<th>Skill Name</th>");
                sb.Append("<th>Grade</th>");
                sb.Append("</tr>");

                foreach (var data in listdata)
                {
                    sb.Append("<tr>");
                    sb.Append("<td width='100'>" + data.SkillName + "</td>");
                    sb.Append("<td width='80'>" + data.Grade + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            else
                sb.Append("");

            return sb.ToString();
        }

    }
}