﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NReco.PdfGenerator;
using System.IO;
using Pci.CvBuilder.Entity;
using System.Text;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Pci.CvBuilder.UI.UIServiceWorker.PageSW;
using Pci.CvBuilder.UI.UIServiceWorker.ConfigSW;
using Pci.CvBuilder.UI.UIServiceWorker.UserSW;
using Pci.CvBuilder.UI.Areas.Common.Models;
using Pci.CommonLib.EFRepositories.Model;


namespace Pci.CvBuilder.UI.Areas.Common.Controllers
{
    public class ListCvController : Controller
    {
        // GET: Common/ListCv
        public ActionResult Index()
        {
            return View();
        }

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult ViewDetail(int id)
        {
            CvBase cvbase = new CvBase();
            ViewBag.Message = id;
            return PartialView("View", cvbase);
        }

        public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        { 
            var listData = new UserServiceWorker().GetUserList().Where(o => o.Name != null).ToList();
            return Json(listData.ToDataSourceResult(request));
        }

        public string RenderViesAsString(string viewName, object model, int id)
        {
            long user = Convert.ToInt32(this.Session["Id"]);
            //Define model to render in html

            CvBase cvBase = GetCvBaseData(id);
            //Create a string writer to receive the HTML code
            StringWriter stringWriter = new StringWriter();

            //Get the view to render
            ViewEngineResult viewResult = ViewEngines.Engines.FindView(ControllerContext,
                viewName, null);

            //Create a context to render a view based on a model
            ViewContext viewContext = new ViewContext(
                ControllerContext,
                viewResult.View,
                new ViewDataDictionary(cvBase),
                new TempDataDictionary(),
                stringWriter);

            //Render the view to a HTML code
            viewResult.View.Render(viewContext, stringWriter);

            //Return the HTML code
            return stringWriter.ToString();
        }

        [ValidateInput(false)]
        public ActionResult GenerateListCVToPDF(int id)
        {
            var htmlToPdf = new HtmlToPdfConverter();

            string htmlContent = RenderViesAsString("CvPage", null, id);
            var pdfContentType = "application/pdf";

            var result = File(htmlToPdf.GeneratePdf(htmlContent, null), pdfContentType);
            return result;
        }
        /// <summary>
        /// Get CVBase Data
        /// </summary>
        /// <param name="idUser"></param>
        /// <returns></returns>
        public CvBase GetCvBaseData(long idUser)
        {
            var data = new PageServiceWorker().GetAllList<User>()
                .Where(o => o.Id == idUser).FirstOrDefault();
            CvBase cvBase = new CvBase();
            cvBase.LogoImage = Server.MapPath("~/Content/Images/logo praweda.png");

            if (data != null)
            {
                cvBase.Name = data.Name;
                cvBase.Gender = data.Gender;
                cvBase.PlaceOfBirth = data.PlaceOfBirth;
                cvBase.DateOfBirth = Convert.ToDateTime(data.DateOfBirth).ToString("dd/MM/yyyy");
                cvBase.Address = data.Address;
                cvBase.MobilePhone = data.MobilePhone;
                cvBase.Email = data.Email;
                var pathImg = "~/Content/Images/ProfilePictures/" + data.Image;
                cvBase.Image = Server.MapPath(pathImg);
                cvBase.Education = new GenerateCVController().EducationGetLoop(idUser);
                cvBase.Experience = new GenerateCVController().ExperienceGetLoop(idUser);
                cvBase.Training = new GenerateCVController().TrainingGetLoop(idUser);
                cvBase.Skill = new GenerateCVController().SkillGetLoop(idUser);

            }
            return cvBase;
        }
    }
}