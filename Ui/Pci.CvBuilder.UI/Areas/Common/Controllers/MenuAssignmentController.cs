﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Pci.CommonLib.EFRepositories.Model;
using Pci.CvBuilder.UI.UIServiceWorker.PageSW;
using Pci.CvBuilder.UI.UIServiceWorker.MenuAssignmentSW;
using Pci.CvBuilder.Entity;
using Pci.CvBuilder.UI.Areas.Common.Models;

namespace Pci.CvBuilder.UI.Areas.Common.Controllers
{
    public class MenuAssignmentController : Controller
    {
        // GET: Common/MenuAssignment
        public ActionResult Index()
        {
            ViewBag.Header = "Menu Assignment";
            return View();
        }

        [HttpGet]
        public ActionResult ViewDetail(long id)
        {
            ViewBag.Action = "View";
            MenuAsssignmentApproval menuAssingment = new PageServiceWorker().GetDetailData<MenuAsssignmentApproval>(id);
            return View("Detail", menuAssingment);

        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Action = "Create";
            MenuAsssignmentApproval menuAssignment = new MenuAsssignmentApproval();

            return View("Detail", menuAssignment);
        }

        [HttpGet]
        public ActionResult Update(long id)
        {
            ViewBag.Action = "Update";
            MenuAsssignmentApproval menuAssignment = new PageServiceWorker().GetDetailData<MenuAsssignmentApproval>(id);
            return View("Detail", menuAssignment);
        }

        /// <summary>
        /// Read data to show in grid
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ActionResult Read(GridState request)
        {
            return Json(new PageServiceWorker().GetPageList<MenuAsssignmentApproval>(request));
        }

        /// <summary>
        /// Create menu assignment
        /// </summary>
        /// <param name="userGroupId"></param>
        /// <param name="treeData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(long userGroupId, string TreeData )
        {
            try
            {
                var service = new PageServiceWorker();
                var menuSW = new MenuAssignmentServiceWorker();
                var menuApproval = new MenuAsssignmentApproval();
                menuApproval.UserGroupId = userGroupId;
                var allMenu = service.GetAllList<Menu>();
                MenuMap.GetAssignedMenu(menuApproval, userGroupId, TreeData, allMenu);
                menuSW.CreateMenuAssignment(menuApproval);

                return Json(new { success = true });
            }
            catch(Exception e)
            {
                return Json(new { error = "Error! Can't Save Data!" + e.Message });
            }
        }

        /// <summary>
        /// Update menu assignment
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="userGroupId"></param>
        /// <param name="TreeData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(long Id, long userGroupId, string TreeData)
        {
            try
            {
                var service = new PageServiceWorker();
                var menuSW = new MenuAssignmentServiceWorker();
                var menuApproval = service.GetDetailData<MenuAsssignmentApproval>(Id);
                var allMenu = service.GetAllList<Entity.Menu>();
                menuApproval.MenuAssignment = null;
                MenuMap.GetAssignedMenu(menuApproval, userGroupId, TreeData, allMenu);
                menuSW.UpdateMenuAssignment(menuApproval);

                return Json(new { success = true });
            }
            catch(Exception e)
            {
                return Json(new { error = "Error! Can't save data! " + e.Message });
            }
        }

        /// <summary>
        /// Delete menu assignment
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Destroy(long Id)
        {
            try
            {
                MenuAsssignmentApproval menuAssignmentApp = new PageServiceWorker().GetDetailData<MenuAsssignmentApproval>(Id);
                new MenuAssignmentServiceWorker().DeleteMenuAssignment(menuAssignmentApp);

                return Json(new { success = true });
            }
            catch (Exception e)
            {
                return Json(new { error = "Error! Can't save data! " + e.Message });
            }
        }
        
        public ActionResult RetrieveCompleteTree(long? userGroupId)
        {
            var service = new PageServiceWorker();
            var allMenu = service.GetAllList<Entity.Menu>();
            var assignedMenus = new List<MenuAssignment>();
            if (userGroupId != null && userGroupId.HasValue)
            {
                assignedMenus = new MenuAssignmentServiceWorker().GetMenuByUserGroupId(userGroupId.Value);
            }
            var root = new MenuMap();
            MenuMap.PopulateTreeMap(root, allMenu, assignedMenus);
            var strJson = JsonConvert.SerializeObject(root.Items);

            return new ContentResult { Content = strJson, ContentType = "application/json" };
        }
    }
}