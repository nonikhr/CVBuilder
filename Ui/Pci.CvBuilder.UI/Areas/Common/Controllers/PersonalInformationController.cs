﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pci.CommonLib.EFRepositories.Model;
using Pci.CvBuilder.UI.UIServiceWorker.PageSW;
using Pci.CvBuilder.UI.UIServiceWorker.PersonalInformationSW;
using Pci.CvBuilder.UI.UIServiceWorker.UserSW;
using Pci.CvBuilder.Entity;
using System.IO;

namespace Pci.CvBuilder.UI.Areas.Common.Controllers
{
    public class PersonalInformationController : Controller
    {
        // GET: Common/PersonalInformation
        public ActionResult Index()
        {
            var ids = Session["Id"].ToString();
            User user = new User();
            if (ids != null)
            {
                int id = Int32.Parse(ids);
                user = new PageServiceWorker().GetDetailData<User>(id);
            }
            return View(user);
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult UpdateData(HttpPostedFileBase files, User request)
        {
            try
            {
                User user = new User();

                if (files != null && files.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(files.FileName);
                    var physicalPath = Path.Combine(Server.MapPath("~/Content/Images/ProfilePictures"), fileName);
                    files.SaveAs(physicalPath);
                    user.Image = files.FileName;
                }
                else
                {
                    // Get old Image data
                    var imgDataOld = new UserServiceWorker().GetUserList().Where(o => o.Id == request.Id)
                        .FirstOrDefault().Image;
                    user.Image = imgDataOld;
                }

                user.Id = request.Id;
                user.UserId = request.UserId;
                user.UserName = request.UserName;
                user.Password = request.Password;
                user.UserGroup = request.UserGroup;
                user.Name = request.Name;
                user.Gender = request.Gender;
                user.PlaceOfBirth = request.PlaceOfBirth;
                user.DateOfBirth = request.DateOfBirth;
                user.Address = request.Address;
                user.MobilePhone = request.MobilePhone;
                user.Email = request.Email;

                new PersonalInformationServiceWorker().UpdatePersonalInformation(user);
                @ViewBag.Message = "Success upload file";
            }
            catch (Exception ex)
            {
                @ViewBag.Message = "ERROR:" + ex.Message.ToString();
            }
            return RedirectToAction("Index");
        }
    }
}