﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Pci.CommonLib.EFRepositories.Model;
using Pci.CvBuilder.UI.UIServiceWorker.PageSW;
using Pci.CvBuilder.UI.UIServiceWorker.SkillSW;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.UI.Areas.Common.Controllers
{
    public class SkillController : Controller
    {
        // GET: Common/Skill
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Read data for gridview
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        {
            long id = Convert.ToInt32(this.Session["id"]);
            var listData = new SkillServiceWorker().getSkillList().Where(o => o.UserId == id).ToList();
            return Json(listData.ToDataSourceResult(request));
        }

        /// <summary>
        /// Detail Skill
        /// </summary>
        /// <returns></returns>
        public ActionResult Detail() {
            Skill skill = new Skill();
            return View(skill);
        }

        /// <summary>
        /// Update Skill 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Update(int id) {
            Skill skill = new Skill();
            skill = new PageServiceWorker().GetDetailData<Skill>(id);
            return View(skill);
        }

        /// <summary>
        /// View Skill
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult View(int id) {
            Skill skill = new Skill();
            skill = new PageServiceWorker().GetDetailData<Skill>(id);
            return View(skill);
        }

        /// <summary>
        /// Create Skill
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(Skill request)
        {
            try
            {
                var userId = this.Session["Id"].ToString();

                Skill skill = new Skill();
                skill.UserId = int.Parse(userId);
                skill.SkillName = request.SkillName;
                skill.Grade = request.Grade;

                new SkillServiceWorker().CreateSkill(skill);
                return Json(new { success = true });
            }
            catch {
                throw;
            }
        }

        /// <summary>
        /// Update Skill
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateData(Skill request)
        {
            try
            {
                var userId = this.Session["Id"].ToString();
                Skill skill = new Skill();
                skill.Id = request.Id;
                skill.UserId = int.Parse(userId);
                skill.SkillName = request.SkillName;
                skill.Grade = request.Grade;

                new SkillServiceWorker().UpdateSkill(skill);
                return Json(new { success = true });
            }
            catch {
                throw;
            }
        }

        /// <summary>
        /// Delete Skill
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Destroy(int id) {
            try
            {
                Skill skill = new Skill();
                skill = new PageServiceWorker().GetDetailData<Skill>(id);

                new SkillServiceWorker().DeleteSkill(skill);
                return Json(new { success = true });
            }
            catch {
                throw;
            }
        }
    }
}