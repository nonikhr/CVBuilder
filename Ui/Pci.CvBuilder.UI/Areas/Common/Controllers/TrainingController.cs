﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Pci.CommonLib.EFRepositories.Model;
using Pci.CvBuilder.UI.UIServiceWorker.PageSW;
using Pci.CvBuilder.UI.UIServiceWorker.TrainingSW;
using Pci.CvBuilder.Entity;


namespace Pci.CvBuilder.UI.Areas.Common.Controllers
{
    public class TrainingController : Controller
    {
        // GET: Common/Training
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Read data for gridview
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        {
            long id = Convert.ToInt32(this.Session["Id"]);
            var listData = new TrainingServiceWorker().GetTrainingList().Where(o => o.UserId == id).ToList();
            return Json(listData.ToDataSourceResult(request));
        }

        /// <summary>
        /// Detail 
        /// </summary>
        /// <returns></returns>
        public ActionResult Detail()
        {
            Training training = new Training();
            return View(training);
        }

        /// <summary>
        /// Update view
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Update(int id)
        {
            Training training = new Training();
            training = new PageServiceWorker().GetDetailData<Training>(id);
            return View(training);
        }

        /// <summary>
        /// View detail
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult View(int id)
        {
            Training training = new Training();
            training = new PageServiceWorker().GetDetailData<Training>(id);
            return View(training);
        }

        /// <summary>
        /// Create Training
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(Training request)
        {
            try
            {
                var userId = this.Session["Id"].ToString();

                Training training = new Training();
                training.UserId = int.Parse(userId);
                training.ProgramName = request.ProgramName;
                training.Institution = request.Institution;
                training.Date = request.Date;
                training.Certified = request.Certified;
                // sub string kalau databasenya nChar(1)
                //training.Certified = request.Certified.Substring(0,1);

                new TrainingServiceWorker().CreateTraining(training);
                return Json(new { success = true });
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update Training
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateData(Training request)
        {
            try
            {
                var userId = this.Session["Id"].ToString();
                Training training = new Training();
                training.Id = request.Id;
                training.UserId = int.Parse(userId);
                training.ProgramName = request.ProgramName;
                training.Institution = request.Institution;
                training.Date = request.Date;
                training.Certified = request.Certified;
                //training.Certified = request.Certified.Substring(0,1);

                new TrainingServiceWorker().UpdateTraining(training);
                return Json(new { success = true });
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete data
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Destroy(int id)
        {
            try
            {
                Training training = new Training();
                training = new PageServiceWorker().GetDetailData<Training>(id);

                new TrainingServiceWorker().DeleteTraining(training);
                return Json(new { success = true });
            }
            catch
            {
                throw;
            }
        }

    }
}