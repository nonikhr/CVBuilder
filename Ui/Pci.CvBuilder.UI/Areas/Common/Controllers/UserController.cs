﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pci.CommonLib.EFRepositories.Model;
using Pci.CvBuilder.UI.UIServiceWorker.PageSW;
using Pci.CvBuilder.Entity;
using Pci.CvBuilder.UI.UIServiceWorker.ConfigSW;
using Pci.CvBuilder.UI.UIServiceWorker.UserSW;
using Pci.CvBuilder.UI.Commons;

namespace Pci.CvBuilder.UI.Areas.Common.Controllers
{
    public class UserController : Controller
    {
        // GET: Common/User
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Read data to show in grid
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ActionResult Read(GridState request)
        {
            var listData = new PageServiceWorker().GetPageList<User>(request);
            return Json(listData);
        }

        public ActionResult Detail()
        {
            Entity.User user = new Entity.User();
            return View(user);

        }

        /// <summary>
        /// Update view
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Update(int id)
        {
            Entity.User user = new Entity.User();
            user = new PageServiceWorker().GetDetailData<Entity.User>(id);
            return View(user);
        }

        /// <summary>
        /// View detail
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult View(int id)
        {
            Entity.User user = new Entity.User();
            user = new PageServiceWorker().GetDetailData<Entity.User>(id);
            return View(user);
        }

        /// <summary>
        /// Create user data
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(User request)
        {
            try
            {
                // check if user id already exist
                var userOld = new UserServiceWorker().GetUserList().Where(o => o.UserId.Trim() == request.UserId.Trim()).ToList();

                if (userOld.Count > 0)
                {
                    return Json(new { exist = true });
                }
                else
                {
                    string defaultPassword = new ConfigServiceWorker().GetConfig().DefaultPassword;

                    Entity.User user = new Entity.User();
                    user.UserId = request.UserId;
                    user.UserName = request.UserName;
                    user.Password = Crypto.Encryption(defaultPassword);
                    user.UserGroup = request.UserGroup;

                    new UserServiceWorker().CreateUser(user);
                    return Json(new { success = true });
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update user data
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateData(User request)
        {
            try
            {
                // check if user id already exist
                var userOld = new UserServiceWorker().GetUserList().Where(o => o.UserId.Trim() == request.UserId.Trim()).ToList();

                if (userOld.Count > 0)
                {
                    return Json(new { exist = true });
                }
                else
                {
                    string defaultPassword = new ConfigServiceWorker().GetConfig().DefaultPassword;

                    Entity.User user = new Entity.User();
                    user.Id = request.Id;
                    user.UserId = request.UserId;
                    user.UserName = request.UserName;
                    user.Password = Crypto.Encryption(defaultPassword);
                    user.UserGroup = request.UserGroup;

                    new UserServiceWorker().UpdateUser(user);
                    return Json(new { success = true });
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete data
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Destroy(int id)
        {
            try
            {
                Entity.User user = new Entity.User();
                user = new PageServiceWorker().GetDetailData<Entity.User>(id);

                new UserServiceWorker().DeleteUser(user);
                return Json(new { success = true });
            }
            catch
            {
                throw;
            }
        }
    }
}