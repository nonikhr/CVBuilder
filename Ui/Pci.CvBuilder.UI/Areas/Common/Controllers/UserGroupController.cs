﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pci.CommonLib.EFRepositories.Model;
using Pci.CvBuilder.UI.UIServiceWorker.PageSW;
using Pci.CvBuilder.UI.UIServiceWorker.UserGroupSW;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.UI.Areas.Common.Controllers
{
    public class UserGroupController : Controller
    {
        // GET: Common/UserGroup
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Read data for gridview
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ActionResult Read(GridState request)
        {
            var listData = new PageServiceWorker().GetPageList<UserGroup>(request);
            return Json(listData);
        }

        /// <summary>
        /// Detail 
        /// </summary>
        /// <returns></returns>
        public ActionResult Detail()
        {
            UserGroup userGroup = new UserGroup();
            return View(userGroup);
        }

        /// <summary>
        /// Update view
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Update(int id)
        {
            UserGroup userGrp = new UserGroup();
            userGrp = new PageServiceWorker().GetDetailData<UserGroup>(id);
            return View(userGrp);
        }

        /// <summary>
        /// View detail
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult View(int id)
        {
            UserGroup userGrp = new UserGroup();
            userGrp = new PageServiceWorker().GetDetailData<UserGroup>(id);
            return View(userGrp);
        }

        /// <summary>
        /// Create user gorup
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(UserGroup request)
        {
            try
            {
                UserGroup userGrp = new UserGroup();
                userGrp.Code = request.Code;
                userGrp.Description = request.Description;

                new UserGroupServiceWorker().CreateUserGroup(userGrp);
                return Json(new { success = true });
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update User Group
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateData(UserGroup request)
        {
            try
            {
                UserGroup userGrp = new UserGroup();
                userGrp.Id = request.Id;
                userGrp.Code = request.Code;
                userGrp.Description = request.Description;

                new UserGroupServiceWorker().UpdateUserGroup(userGrp);
                return Json(new { success = true });
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete data
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Destroy(int id)
        {
            try
            {
                UserGroup userGrp = new UserGroup();
                userGrp = new PageServiceWorker().GetDetailData<UserGroup>(id);

                new UserGroupServiceWorker().DeleteUser(userGrp);
                return Json(new { success = true });
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get user group list for combo box
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult GetUserGroupForComboBox()
        {
            var result = new PageServiceWorker().GetAllList<UserGroup>();
            var strJsn = Json(result.Select(c => new { Code = c.Id, Description = c.Code}),
                JsonRequestBehavior.AllowGet);
            return strJsn;
        }
    }
}