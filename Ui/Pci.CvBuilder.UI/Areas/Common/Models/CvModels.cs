﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pci.CvBuilder.UI.Areas.Common.Models
{
    public class CvBase
    {
        public string LogoImage { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string PlaceOfBirth { get; set; }
        public string DateOfBirth { get; set; }
        public string Address { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
        public string Education { get; set; }
        public string Experience { get; set; }
        public string Training { get; set; }
        public string Skill { get; set; }
    }
}