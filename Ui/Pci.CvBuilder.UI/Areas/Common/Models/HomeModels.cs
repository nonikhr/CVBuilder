﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pci.CvBuilder.Entity;
//using System.Web.Mvc;



namespace Pci.CvBuilder.UI.Areas.Common.Models
{
    public class HomeBase
    {
        public string Name { get; set; }
        public string Gender { get; set; }
        public string PlaceOfBirth { get; set; }
        public string DateOfBirth { get; set; }
        public string Address { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
        public List<Education> Education { get; set; }
        public List<Experience> Experience { get; set; }
        public List<Training> Training { get; set; }
        public List<Skill> Skill { get; set; }
        public List<Document> Document { get; set; }

    }
}