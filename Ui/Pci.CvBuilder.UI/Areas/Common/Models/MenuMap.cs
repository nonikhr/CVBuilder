﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.UI.Areas.Common.Models
{
    public class MenuMap
    {
        [JsonProperty(PropertyName = "key")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "title")]
        public string Text { get; set; }
        [JsonProperty(PropertyName = "selected")]
        public bool Checked { get; set; }
        [JsonProperty(PropertyName = "folder")]
        public bool HasChildren { get; set; }
        public string MenuCode { get; set; }
        [JsonProperty(PropertyName = "children")]
        public List<MenuMap> Items { get; set; }
        [JsonProperty(PropertyName = "expanded")]
        public bool Expanded { get; set; }
        public bool IsEnable { get; set; }

        public static void PopulateTreeMap(MenuMap parent,
            IEnumerable<Entity.Menu> allMenu,
            IEnumerable<Entity.MenuAssignment> assignedMenu,
            bool isEnable = true)
        {
            IEnumerable<Entity.Menu> childsMenu;
            parent.IsEnable = isEnable;
            if (string.IsNullOrEmpty(parent.Id))
            {
                parent.Text = string.Empty;
                parent.Checked = false;
                parent.Items = new List<MenuMap>();
                childsMenu = allMenu.Where(o => string.IsNullOrEmpty(o.ParentId) ||
                    string.IsNullOrWhiteSpace(o.ParentId))
                    .OrderBy(o => o.Sequence)
                    .ToList();
            }
            else
            {
                parent.Checked = false;
                parent.Checked = assignedMenu.Where(o => o.MenuId == long.Parse(parent.Id)).Count() > 0;
                childsMenu = allMenu.Where(o => o.ParentId == parent.MenuCode).OrderBy(o => o.Sequence).ToList();
            }

            if (childsMenu != null && childsMenu.Count() > 0)
            {
                parent.HasChildren = true;
            }

            foreach (var menu in childsMenu)
            {
                var childMenu = new MenuMap();
                childMenu.Text = menu.MenuDesc;
                childMenu.Id = menu.Id.ToString();
                childMenu.MenuCode = menu.MenuId;
                childMenu.Expanded = true;
                childMenu.Checked = assignedMenu.Where(o => o.MenuId == menu.Id).Count() > 0;
                childMenu.Items = new List<MenuMap>();
                childMenu.IsEnable = isEnable;
                parent.Items.Add(childMenu);
                PopulateTreeMap(childMenu, allMenu, assignedMenu, isEnable);
            }
        }

        public static void GetAssignedMenu(MenuAsssignmentApproval menuApproval,
            long userGroupId,
            IEnumerable<Entity.Menu> allMenus,
            params long[] assignedMenuIds)
        {
            menuApproval.UserGroupId = userGroupId;
            if (menuApproval.MenuAssignment == null)
            {
                menuApproval.MenuAssignment = new List<MenuAssignment>();
            }

            foreach (var menuId in assignedMenuIds)
            {
                var assignedMenu = allMenus.Where(o => o.Id == menuId).FirstOrDefault();
                var menuAssignment = new MenuAssignment();
                menuAssignment.MenuId = menuId;
                //if (menuAssignment.MenuAssignmentApproval == null)
                //{
                //    menuAssignment.MenuAssignmentApproval = new MenuAssignmentApproval();
                //}

                //menuAssignment.MenuAssignmentApproval.UserGroupId = userGroupId;

                menuApproval.MenuAssignment.Add(menuAssignment);

                var isHasChildren = allMenus.Any(o => o.ParentId == assignedMenu.MenuId);
                if (isHasChildren)
                {
                    var childMenus = allMenus.Where(o => o.ParentId == assignedMenu.MenuId).ToList();
                    foreach (var childMenu in childMenus)
                    {
                        GetAssignedMenu(menuApproval, userGroupId, allMenus, childMenu.Id);
                    }
                }
            }
        }

        public static void GetAssignedSubMenu(MenuAsssignmentApproval menuApproval,
            long userGroupId,
            string strLevel,
            IEnumerable<Entity.Menu> allMenus,
            params long[] assignedMenuIds
            )
        {
            menuApproval.UserGroupId = userGroupId;
            if (menuApproval.MenuAssignment == null)
            {
                menuApproval.MenuAssignment = new List<MenuAssignment>();
            }

            foreach (var menuId in assignedMenuIds)
            {
                var assignedMenu = allMenus.Where(o => o.Id == menuId).FirstOrDefault();
                var menuAssignment = new MenuAssignment();
                menuAssignment.MenuId = menuId;

                menuApproval.MenuAssignment.Add(menuAssignment);


            }
        }

        public static void GetAssignedMenu(MenuAsssignmentApproval menuApproval,
            long userGroupId,
            MenuMap menuMap,
            List<Entity.Menu> allMenus)
        {
            if (menuMap.Checked)
            {
                GetAssignedMenu(menuApproval, userGroupId, allMenus, long.Parse(menuMap.Id));
            }
            else
            {
                if (menuMap.HasChildren)
                {
                    bool checkingFirstLevelChecked = false;

                    foreach (var childMenuMap in menuMap.Items)
                    {

                        //GetAssignedMenu(menuApproval, userGroupId, allMenus, childMenuMap.Id);

                        if (childMenuMap.HasChildren)
                        {
                            bool checkingSecondLevelChecked = false;

                            //foreach (var childMenuMap1 in childMenuMap.Items)
                            //{
                            //    if (childMenuMap1.Checked)
                            //    {
                            //        checkingSecondLevelChecked = true;
                            //        GetAssignedSubMenu(menuApproval, userGroupId, "", allMenus, long.Parse(childMenuMap1.Id));
                            //    }
                            //}

                            foreach (var childMenuMap1 in childMenuMap.Items)
                            {
                                if (childMenuMap1.HasChildren)
                                {
                                    bool checkingThirdLevelChecked = false;

                                    foreach (var childMenuMap2 in childMenuMap1.Items)
                                    {
                                        if (childMenuMap2.Checked)
                                        {
                                            checkingThirdLevelChecked = true;
                                            GetAssignedSubMenu(menuApproval, userGroupId, "", allMenus, long.Parse(childMenuMap2.Id));
                                        }
                                    }

                                    if (checkingThirdLevelChecked)
                                    {
                                        checkingThirdLevelChecked = true;
                                        GetAssignedSubMenu(menuApproval, userGroupId, "", allMenus, long.Parse(childMenuMap1.Id));
                                    }
                                }
                                else
                                {
                                    if (childMenuMap1.Checked)
                                    {
                                        checkingSecondLevelChecked = true;
                                        GetAssignedSubMenu(menuApproval, userGroupId, "", allMenus, long.Parse(childMenuMap1.Id));
                                    }
                                }
                            }

                            if (checkingSecondLevelChecked)
                            {
                                checkingFirstLevelChecked = true;
                                GetAssignedSubMenu(menuApproval, userGroupId, "", allMenus, long.Parse(childMenuMap.Id));
                            }
                        }
                        else
                        {
                            if (childMenuMap.Checked)
                            {
                                checkingFirstLevelChecked = true;
                                //TODO
                                GetAssignedSubMenu(menuApproval, userGroupId, "", allMenus, long.Parse(childMenuMap.Id));
                            }

                        }
                    }

                    if (checkingFirstLevelChecked)
                    {
                        GetAssignedSubMenu(menuApproval, userGroupId, "", allMenus, long.Parse(menuMap.Id));
                    }

                }
            }
        }

        public static void GetAssignedMenu(MenuAsssignmentApproval menuApproval,
            long userGroupId,
            string jsonMenuMap,
            List<Entity.Menu> allMenus)
        {
            MenuMap menuMaps = JsonConvert.DeserializeObject<MenuMap>(jsonMenuMap);
            foreach (var menuMap in menuMaps.Items)
            {
                GetAssignedMenu(menuApproval, userGroupId, menuMap, allMenus);
            }
        }
    }
}