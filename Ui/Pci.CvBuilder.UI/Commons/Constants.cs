﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Pci.CvBuilder.UI.Commons
{
    public class Constants
    {
        public static string uiPageServiceUrl = ConfigurationManager.AppSettings["UIPageServiceUrl"].ToString();
        public static string uiUserServiceUrl = ConfigurationManager.AppSettings["UIUserServiceUrl"].ToString();
        public static string uiConfigServiceUrl = ConfigurationManager.AppSettings["UIConfigServiceUrl"].ToString();
        public static string uiMenuAssignmentServiceUrl = ConfigurationManager.AppSettings["UIMenuAssignmentServiceUrl"].ToString();
        public static string uiUserGroupServiceUrl = ConfigurationManager.AppSettings["UIUserGroupServiceUrl"].ToString();
        public static string uiEducationServiceUrl = ConfigurationManager.AppSettings["UIEducationServiceUrl"].ToString();
        public static string uiExperienceServiceUrl = ConfigurationManager.AppSettings["UIExperienceServiceUrl"].ToString();
        public static string uiTrainingServiceUrl = ConfigurationManager.AppSettings["UITrainingServiceUrl"].ToString();
        public static string uiSkillServiceUrl = ConfigurationManager.AppSettings["UISkillServiceUrl"].ToString();
        public static string uiDocumentServiceUrl = ConfigurationManager.AppSettings["UIDocumentServiceUrl"].ToString();
        public static string uiPersonalInformationServiceUrl = ConfigurationManager.AppSettings["UIPersonalInformationServiceUrl"].ToString();
    }
}