﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace Pci.CvBuilder.UI.Commons
{
    public class Crypto
    {
        public static string Encryption(string pstrPwd)
        {
            string strHex = string.Empty;
            // A hash function works on a byte array, so we will create two arrays, 
            // one for our resulting hash and one for the given text.
            byte[] HashValue, MessageBytes = UnicodeEncoding.Default.GetBytes(pstrPwd);
            // Now we create an object that will hash our text:

            SHA512 sha512 = new SHA512Managed();
            // And finally we calculate the hash and convert it to a hexadecimal string. 
            // Which we can store in a database for example
            HashValue = sha512.ComputeHash(MessageBytes);
            foreach (byte b in HashValue)
                strHex += string.Format("{0:x2}", b);
            return strHex;
        }

        //compare oriPassword entered by user with hashed value from database
        //true if equal, false if failure
        public static bool CheckHash(string oriPassword, string strHashPassword)
        {
            string originalPassword = Encryption(oriPassword);
            return (originalPassword == strHashPassword);
        }
    }
}