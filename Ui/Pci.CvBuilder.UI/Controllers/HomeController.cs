﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text;
using Kendo.Mvc.UI;
using Pci.CvBuilder.Entity;
using Pci.CvBuilder.UI.Areas.Common.Models;
using Pci.CvBuilder.UI.UIServiceWorker.UserSW;
using Pci.CvBuilder.UI.UIServiceWorker.PageSW;
using Pci.CvBuilder.UI.UIServiceWorker.EducationSW;
using Pci.CommonLib.EFRepositories.Model;

namespace Pci.CvBuilder.UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var userId = int.Parse(this.Session["id"] != null ?
                this.Session["Id"].ToString() : "0");
            //var userId = int.Parse(this.Session["Id"].ToString());
            var listed = new PageServiceWorker().GetAllList<Education>().Where(o => o.UserId == userId).ToList();
            var listex = new PageServiceWorker().GetAllList<Experience>().Where(o => o.UserId == userId).ToList();
            var listsk = new PageServiceWorker().GetAllList<Skill>().Where(o => o.UserId == userId).ToList();
            var listtr = new PageServiceWorker().GetAllList<Training>().Where(o => o.UserId == userId).ToList();
            var listdoc = new PageServiceWorker().GetAllList<Document>().Where(o => o.UserId == userId).ToList();
            var listUser = new PageServiceWorker().GetDetailData<User>(userId);
            HomeBase user = new HomeBase();
            user.Education = listed.ToList();
            user.Experience = listex.ToList();
            user.Skill = listsk.ToList();
            user.Training = listtr.ToList();
            user.Document = listdoc.ToList();
            user.Name = listUser.Name;
            user.Gender = listUser.Gender;
            user.PlaceOfBirth = listUser.PlaceOfBirth;
            user.DateOfBirth = Convert.ToDateTime(listUser.DateOfBirth).ToString("dd/MM/yyyy");
            user.Address = listUser.Address;
            user.MobilePhone = listUser.MobilePhone;
            user.Email = listUser.Email;
            var img = new UserServiceWorker().GetUserList().Where(o => o.Id == listUser.Id)
                .FirstOrDefault().Image;
            user.Image = img;

            return View(user);
        }
    }
}