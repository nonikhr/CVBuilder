﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pci.CvBuilder.Entity;
using Pci.CvBuilder.UI.UIServiceWorker.MenuAssignmentSW;
using Pci.CvBuilder.UI.UIServiceWorker.PageSW;

namespace Pci.CvBuilder.UI.Models
{
    public class MenuAccessModel
    {
        public string MenuId { get; set; }
        public string MenuDesc { get; set; }
        public string ParentId { get; set; }
        public string Url { get; set; }
        public int Sequence { get; set; }
        public MenuAccessModel()
        {

        }
    }

    public class MenuAccess
    {

        public List<MenuAccessModel> items;

        public MenuAccess()
        {
            try
            {
                if (HttpContext.Current.Session["UserGroup"] != null)
                {

                    MenuAccessModel menuAccesModel = null;
                    int userGroupId = int.Parse(HttpContext.Current.Session["UserGroup"].ToString());
                    List<Entity.MenuAssignment> listMenuAssignment = new List<MenuAssignment>();
                    MenuAssignmentServiceWorker menuAssignmentSW = new MenuAssignmentServiceWorker();
                    listMenuAssignment = menuAssignmentSW.GetMenuByUserGroupId(userGroupId);
                    if (listMenuAssignment.Count > 0)
                    {
                        items = new List<MenuAccessModel>();

                        foreach (MenuAssignment mnuAssigment in listMenuAssignment)
                        {
                            menuAccesModel = new MenuAccessModel();
                            menuAccesModel.MenuId = mnuAssigment.Menu.MenuId;
                            menuAccesModel.MenuDesc = mnuAssigment.Menu.MenuDesc;
                            menuAccesModel.ParentId = mnuAssigment.Menu.ParentId;
                            menuAccesModel.Url = mnuAssigment.Menu.Url;
                            menuAccesModel.Sequence = int.Parse(mnuAssigment.Menu.Sequence.ToString());

                            items.Add(menuAccesModel);

                        }
                    }
                }
                else
                {
                    HttpContext.Current.Response.Redirect("/");
                }
            }
            catch
            {
                HttpContext.Current.Response.Redirect("/");
            }
            //try
            //{
            //    if (HttpContext.Current.Session["UserGroup"] != null)
            //    {

            //        MenuAccessModel menuAccesModel = null;
            //        MenuAccessModel menuAccesModelFirst = null;
            //        MenuAccessModel menuAccesModelSecond = null;

            //        int userGroupId = int.Parse(HttpContext.Current.Session["UserGroup"].ToString());
            //        List<Entity.MenuAssignment> listMenuAssignment = new List<MenuAssignment>();

            //        PageServiceWorker pageSW = new PageServiceWorker();
            //        MenuAssignmentServiceWorker menuAssignmentSW = new MenuAssignmentServiceWorker();
            //        List<Menu> listMenu = new List<Menu>();
            //        listMenu = pageSW.GetAllList<Menu>();
            //        listMenuAssignment = menuAssignmentSW.GetMenuByUserGroupId(userGroupId);
            //        if (listMenuAssignment.Count > 0)
            //        {
            //            items = new List<MenuAccessModel>();
            //            int test = 0;
            //            foreach (MenuAssignment mnuAssigment in listMenuAssignment.OrderBy(o => o.Menu.Sequence))
            //            {
            //                test++;
            //                menuAccesModel = new MenuAccessModel();

            //                if ((mnuAssigment.Menu.ParentId != "") && (mnuAssigment.Menu.Url != ""))
            //                {
            //                    var parentMenuId = listMenu.Where(o => o.MenuId == mnuAssigment.Menu.ParentId).SingleOrDefault();
            //                    var parentSubMenuId = items.Where(o => o.MenuId == mnuAssigment.Menu.ParentId).Select(o => o.MenuId).SingleOrDefault();

            //                    if ((parentSubMenuId == "") || (parentSubMenuId == null))
            //                    {
            //                        menuAccesModelFirst = new MenuAccessModel();
            //                        //parent menu
            //                        menuAccesModelFirst.ParentId = parentMenuId.ParentId;
            //                        menuAccesModelFirst.MenuId = parentMenuId.MenuId;
            //                        menuAccesModelFirst.MenuDesc = parentMenuId.MenuDesc;
            //                        menuAccesModelFirst.Url = parentMenuId.Url;
            //                        menuAccesModelFirst.Sequence = int.Parse(parentMenuId.Sequence.ToString());
            //                        items.Add(menuAccesModelFirst);

            //                        menuAccesModelSecond = new MenuAccessModel();
            //                        //sub menu
            //                        menuAccesModelSecond.ParentId = mnuAssigment.Menu.ParentId;
            //                        menuAccesModelSecond.MenuId = mnuAssigment.Menu.MenuId;
            //                        menuAccesModelSecond.MenuDesc = mnuAssigment.Menu.MenuDesc;
            //                        menuAccesModelSecond.Url = mnuAssigment.Menu.Url;
            //                        menuAccesModelSecond.Sequence = int.Parse(mnuAssigment.Menu.Sequence.ToString());
            //                        items.Add(menuAccesModelSecond);
            //                    }
            //                    else
            //                    {
            //                        //sub menu
            //                        menuAccesModel.ParentId = mnuAssigment.Menu.ParentId;
            //                        menuAccesModel.MenuId = mnuAssigment.Menu.MenuId;
            //                        menuAccesModel.MenuDesc = mnuAssigment.Menu.MenuDesc;
            //                        menuAccesModel.Url = mnuAssigment.Menu.Url;
            //                        menuAccesModel.Sequence = int.Parse(mnuAssigment.Menu.Sequence.ToString());
            //                        items.Add(menuAccesModel);
            //                    }

            //                }
            //                else
            //                {
            //                    menuAccesModel.ParentId = mnuAssigment.Menu.ParentId;
            //                    menuAccesModel.MenuId = mnuAssigment.Menu.MenuId;
            //                    menuAccesModel.MenuDesc = mnuAssigment.Menu.MenuDesc;
            //                    menuAccesModel.Url = mnuAssigment.Menu.Url;
            //                    menuAccesModel.Sequence = int.Parse(mnuAssigment.Menu.Sequence.ToString());
            //                    items.Add(menuAccesModel);
            //                }
            //            }
            //        }
            //    }
            //    else
            //    {
            //        HttpContext.Current.Response.Redirect("~/");
            //    }
            //}
            //catch
            //{
            //    HttpContext.Current.Response.Redirect("~/");
            //}
        }
    }
}