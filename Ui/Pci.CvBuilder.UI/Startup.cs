﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Pci.CvBuilder.UI.Startup))]
namespace Pci.CvBuilder.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
