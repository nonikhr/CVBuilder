﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.Infrastructure.Common;
using Pci.CvBuilder.Utility.Wcf.Client;
using Pci.CvBuilder.UI.Commons;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.UI.UIServiceWorker.ConfigSW
{
    public class ConfigServiceWorker
    {
        private readonly IUIConfigurationParameterServices service;
        private readonly ILogger logger;
        private readonly string userName;

        public ConfigServiceWorker()
        {
            this.logger = new Logging();
            this.logger.Init(this.GetType().FullName);
            try
            {
                var channel = new WcfWorkerHelper().CreateChannelFactory<IUIConfigurationParameterServices>();
                var endPointAddress = new WcfWorkerHelper().CreateEndPointAddress(Constants.uiConfigServiceUrl);
                this.service = channel.CreateChannel(endPointAddress);
                this.userName = string.Empty;
                if (HttpContext.Current.Session["User"] != null)
                {
                    this.userName = HttpContext.Current.Session["User"].ToString();
                }
            }
            catch (Exception e)
            {
                logger.LogOnError("Create Ui Service WCF Channel", e);
            }
        }

        /// <summary>
        /// Update config
        /// </summary>
        /// <param name="config"></param>
        public void UpdateConfig(Config config)
        {
            try
            {
                service.UpdateConfig(config);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get config
        /// </summary>
        /// <returns></returns>
        public Config GetConfig()
        {
            Config config = new Config();

            try
            {
                String jsonData = service.GetParameterConfiguration();
                config = JsonConvert.DeserializeObject<Config>(jsonData);
            }
            catch
            {
                throw;
            }

            return config;
        }
    }
}