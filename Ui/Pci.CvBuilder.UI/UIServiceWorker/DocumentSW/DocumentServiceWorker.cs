﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.Infrastructure.Common;
using Pci.CvBuilder.Utility.Wcf.Client;
using Pci.CvBuilder.UI.Commons;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.UI.UIServiceWorker.DocumentSW
{
    public class DocumentServiceWorker
    {
        private readonly IUIDocumentServices service;
        private readonly ILogger logger;
        private readonly string userName;

        public DocumentServiceWorker()
        {
            this.logger = new Logging();
            this.logger.Init(this.GetType().FullName);
            try
            {
                var channel = new WcfWorkerHelper().CreateChannelFactory<IUIDocumentServices>();
                var endPointAddress = new WcfWorkerHelper().CreateEndPointAddress(Constants.uiDocumentServiceUrl);
                this.service = channel.CreateChannel(endPointAddress);
                this.userName = string.Empty;

                if (HttpContext.Current.Session["User"] != null)
                {
                    this.userName = HttpContext.Current.Session["User"].ToString();
                }
            }
            catch (Exception e)
            {
                logger.LogOnError("Create UI Service WCF Channel", e);
            }
        }

        /// <summary>
        /// Create Document
        /// </summary>
        /// <param name="document"></param>
        public void CreateDocument(Document document)
        {
            try { service.Create(document); }
            catch { throw; }
        }

        /// <summary>
        /// Update document
        /// </summary>
        /// <param name="document"></param>
        public void UpdateDocument(Document document)
        {
            try { service.Update(document); }
            catch { throw; }
        }

        /// <summary>
        /// Delete Document
        /// </summary>
        /// <param name="document"></param>
        public void DeleteDocument(Document document)
        {
            try { service.Delete(document); }
            catch { throw; }
        }

        public Document GetDocumentGoupById(int id)
        {
            Document document = new Document();
            try
            {
                string jsonData = service.GetDocumentById(id);
                document = JsonConvert.DeserializeObject<Document>(jsonData);
            }
            catch { throw; }
            return document;
        }

        public List<Document> GetDocumentList()
        {
            List<Document> listData = new List<Document>();
            try
            {
                String jsonData = service.GetDocumentList();
                listData = JsonConvert.DeserializeObject<List<Document>>(jsonData);
            }
            catch { throw; }
            return listData;
        }
    }
}