﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.Infrastructure.Common;
using Pci.CvBuilder.Utility.Wcf.Client;
using Pci.CvBuilder.UI.Commons;
using Pci.CvBuilder.Entity;


namespace Pci.CvBuilder.UI.UIServiceWorker.EducationSW
{
    public class EducationServiceWorker
    {
        private readonly IUIEducationServices service;
        private readonly ILogger logger;
        private readonly string userName;

        public EducationServiceWorker()
        {
            this.logger = new Logging();
            this.logger.Init(this.GetType().FullName);
            try
            {
                var channel = new WcfWorkerHelper().CreateChannelFactory<IUIEducationServices>();
                var endPointAddress = new WcfWorkerHelper().CreateEndPointAddress(Constants.uiEducationServiceUrl);
                this.service = channel.CreateChannel(endPointAddress);
                this.userName = string.Empty;

                if(HttpContext.Current.Session["User"] != null)
                {
                    this.userName = HttpContext.Current.Session["User"].ToString();
                }
            }
            catch(Exception e)
            {
                logger.LogOnError("Create UI servce WCF Channel", e);
            }
        }

        /// <summary>
        /// Create user group
        /// </summary>
        /// <param name="userGroup"></param>
        public void CreateEducation(Education education)
        {
            try
            {
                service.Create(education);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update User group
        /// </summary>
        /// <param name="userGroup"></param>
        public void UpdateEducation(Education education)
        {
            try
            {
                service.Update(education);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete user group
        /// </summary>
        /// <param name="userGroup"></param>
        public void DeleteEducation(Education education)
        {
            try
            {
                service.Delete(education);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get user group by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Education GetEducationGroupById(int id)
        {
            Education education = new Education();

            try
            {
                String jsonData = service.GetEducationById(id);
                education = JsonConvert.DeserializeObject<Education>(jsonData);
            }
            catch
            {
                throw;
            }

            return education;
        }

        /// <summary>
        /// get User group list
        /// </summary>
        /// <returns></returns>
        public List<Education> GetEducationList()
        {
            List<Education> listData = new List<Education>();

            try
            {
                String jsonData = service.GetEducationList();
                listData = JsonConvert.DeserializeObject<List<Education>>(jsonData);
            }
            catch
            {
                throw;
            }

            return listData;
        }
    }
}