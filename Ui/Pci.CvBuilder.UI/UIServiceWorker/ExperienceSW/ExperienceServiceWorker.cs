﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.Infrastructure.Common;
using Pci.CvBuilder.Utility.Wcf.Client;
using Pci.CvBuilder.UI.Commons;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.UI.UIServiceWorker.ExperienceSW
{
    public class ExperienceServiceWorker
    {
        private readonly IUIExperienceServices service;
        private readonly ILogger logger;
        private readonly string userName;

        public ExperienceServiceWorker()
        {
            this.logger = new Logging();
            this.logger.Init(this.GetType().FullName);
            try
            {
                var channel = new WcfWorkerHelper().CreateChannelFactory<IUIExperienceServices>();
                var endPointAddress = new WcfWorkerHelper().CreateEndPointAddress(Constants.uiExperienceServiceUrl);
                this.service = channel.CreateChannel(endPointAddress);
                this.userName = string.Empty;

                if(HttpContext.Current.Session["User"] != null)
                {
                    this.userName = HttpContext.Current.Session["User"].ToString();
                }
            }
            catch(Exception e)
            {
                logger.LogOnError("Create UI servce WCF Channel", e);
            }
        }

        /// <summary>
        /// Create Experience
        /// </summary>
        /// <param name="userGroup"></param>
        public void CreateExperience(Experience experience)
        {
            try
            {
                service.Create(experience);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update Experience
        /// </summary>
        /// <param name="userGroup"></param>
        public void UpdateExperience(Experience experience)
        {
            try
            {
                service.Update(experience);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete Experience
        /// </summary>
        /// <param name="userGroup"></param>
        public void DeleteExperience(Experience experience)
        {
            try
            {
                service.Delete(experience);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get Experience by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Experience GetExperienceGroupById(int id)
        {
            Experience experience = new Experience();

            try
            {
                String jsonData = service.GetExperienceById(id);
                experience = JsonConvert.DeserializeObject<Experience>(jsonData);
            }
            catch
            {
                throw;
            }

            return experience;
        }

        /// <summary>
        /// get Experience list
        /// </summary>
        /// <returns></returns>
        public List<Experience> GetExperienceList()
        {
            List<Experience> listData = new List<Experience>();

            try
            {
                String jsonData = service.GetExperienceList();
                listData = JsonConvert.DeserializeObject<List<Experience>>(jsonData);
            }
            catch
            {
                throw;
            }

            return listData;
        }
    }
}