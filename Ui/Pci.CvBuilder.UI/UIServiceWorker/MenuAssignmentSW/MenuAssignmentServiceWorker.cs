﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.Infrastructure.Common;
using Pci.CvBuilder.Utility.Wcf.Client;
using Pci.CvBuilder.UI.Commons;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.UI.UIServiceWorker.MenuAssignmentSW
{
    public class MenuAssignmentServiceWorker
    {
        private readonly IUIMenuAssignmentServices service;
        private readonly ILogger logger;
        private readonly string userName;

        public MenuAssignmentServiceWorker()
        {
            this.logger = new Logging();
            this.logger.Init(this.GetType().FullName);

            try
            {
                var channel = new WcfWorkerHelper().CreateChannelFactory<IUIMenuAssignmentServices>();
                var endPointAddress = new WcfWorkerHelper().CreateEndPointAddress(Constants.uiMenuAssignmentServiceUrl);
                this.service = channel.CreateChannel(endPointAddress);
                this.userName = string.Empty;
                if (HttpContext.Current.Session["User"] != null)
                {
                    this.userName = HttpContext.Current.Session["User"].ToString();
                }
            }
            catch (Exception e)
            {
                logger.LogOnError("Create UI Service WCF Channel", e);
            }
        }

        /// <summary>
        /// Create menu assignment
        /// </summary>
        /// <param name="menuAssignment"></param>
        public void CreateMenuAssignment(MenuAsssignmentApproval menuAssignment)
        {
            try
            {
                service.CreateMenuAssignment(menuAssignment);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update menu assignment
        /// </summary>
        /// <param name="menuAssignment"></param>
        public void UpdateMenuAssignment(MenuAsssignmentApproval menuAssignment)
        {
            try
            {
                service.UpdateMenuAssignment(menuAssignment);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete menu assignment
        /// </summary>
        /// <param name="menuAssignment"></param>
        public void DeleteMenuAssignment(MenuAsssignmentApproval menuAssignment)
        {
            try
            {
                service.DeleteMenuAssignment(menuAssignment);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get menu assignment by user group id
        /// </summary>
        /// <param name="userGroupId"></param>
        /// <returns></returns>
        public List<MenuAssignment> GetMenuByUserGroupId(long userGroupId)
        {
            List<MenuAssignment> listMenuAssignment = new List<MenuAssignment>();

            try
            {
                String jsonData = service.GetMenuByUserGroupId(userGroupId);
                listMenuAssignment = JsonConvert.DeserializeObject<List<MenuAssignment>>(jsonData);
            }
            catch
            {
                throw;
            }
            return listMenuAssignment;
        }

        /// <summary>
        /// Get menu id
        /// </summary>
        /// <param name="urlClassName"></param>
        /// <returns></returns>
        //public string GetMenuId(string urlClassName)
        //{
        //    string result = string.Empty;

        //    try
        //    {
        //        result = service.GetMenuId(urlClassName);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return result;
        //}
    }
}