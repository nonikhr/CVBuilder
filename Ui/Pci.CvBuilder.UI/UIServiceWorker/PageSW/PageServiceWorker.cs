﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using Kendo.Mvc.UI;
using Kendo.Mvc.Infrastructure;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CvBuilder.Utility.Wcf.Client;
using Pci.CvBuilder.Infrastructure.Common;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.UI.Commons;
using Pci.CvBuilder.Message;
using Pci.CommonLib.EFRepositories.Model;

namespace Pci.CvBuilder.UI.UIServiceWorker.PageSW
{
    public class PageServiceWorker
    {
        private readonly IUIPageServices service;
        private readonly ILogger logger;
        private readonly string userName;

        public PageServiceWorker()
        {
            this.logger = new Logging();
            this.logger.Init(this.GetType().FullName);
            try
            {
                var channel = new WcfWorkerHelper().CreateChannelFactory<IUIPageServices>();
                var endPointAddress = new WcfWorkerHelper().CreateEndPointAddress(Constants.uiPageServiceUrl);
                this.service = channel.CreateChannel(endPointAddress);
                this.userName = string.Empty;
                if(HttpContext.Current.Session["User"] != null)
                {
                    this.userName = HttpContext.Current.Session["User"].ToString();
                }
               
            }
            catch(Exception e)
            {
                logger.LogOnError("Create UI Service WCF Channel", e);
            }
        }

        /// <summary>
        /// Get detail data
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetDetailData<T>(long id)
        {
            var result = service.GetDetailData(new DetailRequest()
                {
                    Id = id,
                    Type = typeof(T).AssemblyQualifiedName,
                    UserName = this.userName
                });

            return JsonConvert.DeserializeObject<T>(result);
        }

        /// <summary>
        /// Get all list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public List<T> GetAllList<T>()
        {
            try
            {
                var request = new ListRequest()
                {
                    Type = typeof(T).AssemblyQualifiedName,
                    UserName = this.userName
                };

                return JsonConvert.DeserializeObject<List<T>>(service.GetAllList(request));
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get Page List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <returns></returns>
        public DataSourceResult GetPageList<T>(GridState request)
            where T : class
        {
            if (request.PageSize == 0)
            {
                request.PageSize = 5;
            }

            var lFilter = new List<string>();
            var lType = new List<string>();

            var result = service.GetPageData(new PageRequest()
            {
                Request = request,
                Type = typeof(T).AssemblyQualifiedName,
                UserName = this.userName
            });

            var kendoResult = JsonConvert.DeserializeObject<KendoPageDataResult<T>>(result);

            return new DataSourceResult()
            {
                AggregateResults = kendoResult.AggregateResults,
                Data = kendoResult.Data,
                Errors = kendoResult.Error,
                Total = kendoResult.Total
            };
        }
    }
}