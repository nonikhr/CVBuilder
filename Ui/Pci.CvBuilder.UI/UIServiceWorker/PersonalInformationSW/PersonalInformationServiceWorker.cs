﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.Infrastructure.Common;
using Pci.CvBuilder.Utility.Wcf.Client;
using Pci.CvBuilder.UI.Commons;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.UI.UIServiceWorker.PersonalInformationSW
{
    public class PersonalInformationServiceWorker
    {
        private readonly IUIPersonalInformationServices service;
        private readonly ILogger logger;
        private readonly string userName;

        public PersonalInformationServiceWorker() { 
            this.logger = new Logging();
            this.logger.Init(this.GetType().FullName);

            try
            {
                var channel = new WcfWorkerHelper().CreateChannelFactory<IUIPersonalInformationServices>();
                var endPointAddress = new WcfWorkerHelper().CreateEndPointAddress(Constants.uiPersonalInformationServiceUrl);
                this.service = channel.CreateChannel(endPointAddress);
                this.userName = string.Empty;

                if (HttpContext.Current.Session["User"] != null)
                {
                    this.userName = HttpContext.Current.Session["User"].ToString();
                }
            }
            catch (Exception e)
            {
                logger.LogOnError("Create UI Service WCF Channel", e);
            }
        }
        
        /// <summary>
        /// Create Personal Information
        /// </summary>
        /// <param name="skill"></param>
        //public void CreatePersonalInformation(PersonalInformation personalInformation)
        //{
        //    try
        //    {
        //        service.Create(personalInformation);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        /// <summary>
        /// Update personal Information
        /// </summary>
        /// <param name="personalInformation"></param>
        public void UpdatePersonalInformation(User user)
        {
            try
            {
                service.Update(user);
            }
            catch
            {
                throw;
            }
        }

        //public PersonalInformation GetPersonalInformationGroupById(int id)
        //{
        //    PersonalInformation personalInformation = new PersonalInformation();
        //    try
        //    {
        //        String jsonData = service.GetPersonalInformationById(id);
        //        personalInformation = JsonConvert.DeserializeObject<PersonalInformation>(jsonData);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return personalInformation;
        //}
        }
    }