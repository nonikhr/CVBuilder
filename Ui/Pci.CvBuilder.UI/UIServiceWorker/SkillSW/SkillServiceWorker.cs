﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.Infrastructure.Common;
using Pci.CvBuilder.Utility.Wcf.Client;
using Pci.CvBuilder.UI.Commons;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.UI.UIServiceWorker.SkillSW
{
    public class SkillServiceWorker
    {
        private readonly IUISkillServices service;
        private readonly ILogger logger;
        private readonly string userName;

        public SkillServiceWorker()
        {
            this.logger = new Logging();
            this.logger.Init(this.GetType().FullName);
            try
            {
                var channel = new WcfWorkerHelper().CreateChannelFactory<IUISkillServices>();
                var endPointAddress = new WcfWorkerHelper().CreateEndPointAddress(Constants.uiSkillServiceUrl);
                this.service = channel.CreateChannel(endPointAddress);
                this.userName = string.Empty;

                if (HttpContext.Current.Session["User"] != null)
                {
                    this.userName = HttpContext.Current.Session["User"].ToString();
                }
            }
            catch (Exception e)
            {
                logger.LogOnError("Create UI Service WCF Channel", e);
            }
        }
        
        /// <summary>
        /// Create Skill
        /// </summary>
        /// <param name="skill"></param>
        public void CreateSkill(Skill skill)
        {
            try
            {
                service.Create(skill);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update Skill
        /// </summary>
        /// <param name="skill"></param>
        public void UpdateSkill(Skill skill)
        {
            try
            {
                service.Update(skill);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete Skill
        /// </summary>
        /// <param name="skill"></param>
        public void DeleteSkill(Skill skill)
        {
            try
            {
                service.Delete(skill);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get Skill by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Skill GetSkillGroupById(int id)
        {
            Skill skill = new Skill();
            try
            {
                String jsonData = service.GetSkillById(id);
                skill = JsonConvert.DeserializeObject<Skill>(jsonData);
            }
            catch
            {
                throw;
            }
            return skill;
        }

        public List<Skill> getSkillList() {
            List<Skill> listData = new List<Skill>();
            try
            {
                String jsonData = service.GetSkillList();
                listData = JsonConvert.DeserializeObject<List<Skill>>(jsonData);
            }
            catch
            {
                throw;
            }

            return listData;
        }
    }
}