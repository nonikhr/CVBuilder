﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.Infrastructure.Common;
using Pci.CvBuilder.Utility.Wcf.Client;
using Pci.CvBuilder.UI.Commons;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.UI.UIServiceWorker.TrainingSW
{
    public class TrainingServiceWorker
    {
        private readonly IUITrainingServices service;
        private readonly ILogger logger;
        private readonly string userName;

        public TrainingServiceWorker()
        {
            this.logger = new Logging();
            this.logger.Init(this.GetType().FullName);
            try
            {
                var channel = new WcfWorkerHelper().CreateChannelFactory<IUITrainingServices>();
                var endPointAddress = new WcfWorkerHelper().CreateEndPointAddress(Constants.uiTrainingServiceUrl);
                this.service = channel.CreateChannel(endPointAddress);
                this.userName = string.Empty;

                if (HttpContext.Current.Session["User"] != null)
                {
                    this.userName = HttpContext.Current.Session["User"].ToString();
                }
            }
            catch (Exception e)
            {
                logger.LogOnError("Create UI service WCF Channel", e);
            }
        }

        /// <summary>
        /// Create Training
        /// </summary>
        /// <param name="training"></param>
        public void CreateTraining(Training training)
        {
            try
            {
                service.Create(training);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update Training
        /// </summary>
        /// <param name="training"></param>
        public void UpdateTraining(Training training)
        {
            try
            {
                service.Update(training);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete Training
        /// </summary>
        /// <param name="training"></param>
        public void DeleteTraining(Training training)
        {
            try
            {
                service.Delete(training);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get Id Training
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Training GetTrainingGroupById(int id)
        {
            Training training = new Training();
            try
            {
                string jsonData = service.GetTrainingById(id);
                training = JsonConvert.DeserializeObject<Training>(jsonData);
            }
            catch {
                throw;
            }
            return training;
        }

        public List<Training> GetTrainingList() {
            List<Training> listData = new List<Training>();

            try
            {
                string jsonData = service.GetTrainingList();
                listData = JsonConvert.DeserializeObject<List<Training>>(jsonData);
            }
            catch {
                throw;
            }
            return listData;
        }
    }
}