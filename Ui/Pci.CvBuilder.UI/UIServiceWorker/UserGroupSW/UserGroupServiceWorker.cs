﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CvBuilder.Infrastructure.Common;
using Pci.CvBuilder.Utility.Wcf.Client;
using Pci.CvBuilder.UI.Commons;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.UI.UIServiceWorker.UserGroupSW
{
    public class UserGroupServiceWorker
    {
        private readonly IUIUserGroupServices service;
        private readonly ILogger logger;
        private readonly string userName;

        public UserGroupServiceWorker()
        {
            this.logger = new Logging();
            this.logger.Init(this.GetType().FullName);
            try
            {
                var channel = new WcfWorkerHelper().CreateChannelFactory<IUIUserGroupServices>();
                var endPointAddress = new WcfWorkerHelper().CreateEndPointAddress(Constants.uiUserGroupServiceUrl);
                this.service = channel.CreateChannel(endPointAddress);
                this.userName = string.Empty;

                if(HttpContext.Current.Session["User"] != null)
                {
                    this.userName = HttpContext.Current.Session["User"].ToString();
                }
            }
            catch(Exception e)
            {
                logger.LogOnError("Create UI servce WCF Channel", e);
            }
        }

        /// <summary>
        /// Create user group
        /// </summary>
        /// <param name="userGroup"></param>
        public void CreateUserGroup(UserGroup userGroup)
        {
            try
            {
                service.Create(userGroup);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update User group
        /// </summary>
        /// <param name="userGroup"></param>
        public void UpdateUserGroup(UserGroup userGroup)
        {
            try
            {
                service.Update(userGroup);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete user group
        /// </summary>
        /// <param name="userGroup"></param>
        public void DeleteUser(UserGroup userGroup)
        {
            try
            {
                service.Delete(userGroup);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get user group by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserGroup GetUserGroupById(int id)
        {
            UserGroup userGroup = new UserGroup();

            try
            {
                String jsonData = service.GetUserGroupById(id);
                userGroup = JsonConvert.DeserializeObject<UserGroup>(jsonData);
            }
            catch
            {
                throw;
            }

            return userGroup;
        }

        /// <summary>
        /// get User group list
        /// </summary>
        /// <returns></returns>
        public List<UserGroup> GetUserGroupList()
        {
            List<UserGroup> listData = new List<UserGroup>();

            try
            {
                String jsonData = service.GetUserGroupList();
                listData = JsonConvert.DeserializeObject<List<UserGroup>>(jsonData);
            }
            catch
            {
                throw;
            }

            return listData;
        }

    }
}