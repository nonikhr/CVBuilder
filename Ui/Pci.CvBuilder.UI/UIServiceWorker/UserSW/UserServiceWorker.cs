﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Pci.CvBuilder.WcfServices;
using Pci.CommonLib.Infrastructure.Logging;
using Pci.CommonLib.ChannelFactory;
using Pci.CvBuilder.Infrastructure.Common;
using Pci.CvBuilder.Utility.Wcf.Client;
using Pci.CvBuilder.UI.Commons;
using Pci.CvBuilder.Entity;

namespace Pci.CvBuilder.UI.UIServiceWorker.UserSW
{
    public class UserServiceWorker
    {
        private readonly IUIUserServices service;
        private readonly ILogger logger;
        private readonly string userName;
        //private readonly string hostName;
        // matra asdw
        /// <summary>
        /// Service worker as the client host to access the service host via WCF
        /// </summary>
        public UserServiceWorker()
        {
            this.logger = new Logging();
            this.logger.Init(this.GetType().FullName);
            try
            {
                var channel = new WcfWorkerHelper().CreateChannelFactory<IUIUserServices>();
                var endPointAddress = new WcfWorkerHelper().CreateEndPointAddress(Constants.uiUserServiceUrl);
                this.service = channel.CreateChannel(endPointAddress);
                this.userName = string.Empty;
                if(HttpContext.Current.Session["User"] != null)
                {
                    this.userName = HttpContext.Current.Session["User"].ToString();
                }

                //if(HttpContext.Current.Session["HostName"] != null)
                //{
                //    this.hostName = HttpContext.Current.Session["HostName"].ToString();
                //}
            }
            catch(Exception e)
            {
                logger.LogOnError("Create Ui Service WCF Channel", e);
            }
        }

        /// <summary>
        /// Create user
        /// </summary>
        /// <param name="model"></param>
        public void CreateUser(User model)
        {
            try
            {
                service.CreateUser(model);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="model"></param>
        public void UpdateUser(User model)
        {
            try
            {
                service.UpdateUser(model);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="model"></param>
        public void DeleteUser(User model)
        {
            try
            {
                service.DeleteUser(model);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get user access by user Id and password
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public User GetUserByUserId(string userId, string password)
        {
            User user = new User();
            
            try
            {
                String jsonData = service.GetUserByUserId(userId, password);
                user = JsonConvert.DeserializeObject<User>(jsonData);
            }
            catch
            {
                throw;
            }

            return user;
        }

        /// <summary>
        /// Get list user
        /// </summary>
        /// <returns></returns>
        public List<User> GetUserList()
        {
            List<User> userList = new List<User>();

            try
            {
                String jsonData = service.GetUserList();
                userList = JsonConvert.DeserializeObject<List<User>>(jsonData);
            }
            catch
            {
                throw;
            }

            return userList;
        }
    }
}