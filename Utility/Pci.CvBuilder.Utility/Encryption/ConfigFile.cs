﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using Pci.CvBuilder.Utility.Hashed;

namespace Pci.CvBuilder.Utility.Encryption
{
    public class ConfigFile
    {
        public static string ReadSqlConnectionString(string connStringName)
        {
            var connString = ConfigurationManager.ConnectionStrings[connStringName].ConnectionString;
            SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder(connString);
            string configPassword = sqlBuilder.Password;
            string clearPassword = ReadPassword(configPassword);
            sqlBuilder.Password = clearPassword;
            connString = sqlBuilder.ConnectionString;
            if (configPassword == clearPassword)
            {
                sqlBuilder.Password = ReadEncryptedPassword(clearPassword);
                SaveConnectionStringConfig(connStringName, sqlBuilder.ConnectionString);
            }

            return connString;
        }

        public static string ReadEfConnectionString(string connStringName)
        {
            var connString = ConfigurationManager.ConnectionStrings[connStringName].ConnectionString;
            EntityConnectionStringBuilder builder = new EntityConnectionStringBuilder(connString);
            SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder(builder.ProviderConnectionString);
            string configPassword = sqlBuilder.Password;
            string clearPassword = ReadPassword(configPassword);
            sqlBuilder.Password = clearPassword;
            builder.ProviderConnectionString = sqlBuilder.ConnectionString;
            connString = builder.ConnectionString;
            if (configPassword == clearPassword)
            {
                sqlBuilder.Password = ReadEncryptedPassword(clearPassword);
                builder.ProviderConnectionString = sqlBuilder.ConnectionString;
                SaveConnectionStringConfig(connStringName, builder.ConnectionString);
            }

            return connString;
        }


        private static string ReadPassword(string configPassword)
        {
            string hashType = "CVBUILDER".Hash();
            var aesKey = AesUtility.GetPrivateKeyInContainer(hashType);
            var aesIv = AesUtility.GetInitializeVectorInContainer(hashType);

            if (configPassword.StartsWith(hashType))
            {
                var length = hashType.Length;
                string chiperValue = configPassword.Remove(0, length);

                configPassword = AesUtility.DecryptString(chiperValue, aesKey, aesIv);
            }

            return configPassword;

        }

        private static string ReadEncryptedPassword(string clearPassword)
        {
            string hashType = "CVBUILDER".Hash();
            var aesKey = AesUtility.GetPrivateKeyInContainer(hashType);
            var aesIv = AesUtility.GetInitializeVectorInContainer(hashType);

            return hashType + AesUtility.EncryptString(clearPassword, aesKey, aesIv);
        }

        private static void SaveConnectionStringConfig(string connStringName, string connString)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.ConnectionStrings.ConnectionStrings[connStringName].ConnectionString = connString;
            config.Save(ConfigurationSaveMode.Modified, true);
            ConfigurationManager.RefreshSection("connectionStrings");
        }
    }
}
