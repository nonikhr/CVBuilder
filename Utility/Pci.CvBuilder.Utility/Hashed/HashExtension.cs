﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pci.CvBuilder.Utility.Hashed
{
    public static class HashExtension
    {
        public static string Hash(this string str)
        {
            var trnBytes = Encoding.ASCII.GetBytes(str);
            var hash = new MurmurHash2Unsafe();
            return hash.Hash(trnBytes).ToString("x");
        }
    }
}
