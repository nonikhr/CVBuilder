﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Xml.XmlConfiguration;

namespace Pci.CvBuilder.Utility.Wcf.Client
{
    public class WcfWorkerChannel
    {
        public ChannelFactory<T> CreateChannelWeb<T>()
        {
            var tcpb = tcpd();
            return new ChannelFactory<T>(tcpb);
        }
        private NetTcpBinding tcpd()
        {
            var tcpb = new NetTcpBinding();
            tcpb.TransferMode = TransferMode.Streamed;
            tcpb.MaxReceivedMessageSize = int.MaxValue;
            tcpb.MaxBufferSize = int.MaxValue;
            tcpb.ReaderQuotas.MaxStringContentLength = int.MaxValue;
            tcpb.ReaderQuotas.MaxArrayLength = int.MaxValue;
            tcpb.ReaderQuotas.MaxBytesPerRead = int.MaxValue;
            tcpb.SendTimeout = new TimeSpan(0, 30, 0);
            tcpb.ReceiveTimeout = tcpb.SendTimeout;
            tcpb.Security.Mode = SecurityMode.None;
            tcpb.ReceiveTimeout = new TimeSpan(0, 30, 0);
            return tcpb;
        }
    }
}
