﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace Pci.CvBuilder.Utility.Wcf.Client
{
    public class WcfWorkerHelper
    {
        public ChannelFactory<T> CreateChannelFactory<T>()
        {
            ChannelFactory<T> channel;
            channel = new WcfWorkerChannel().CreateChannelWeb<T>();
            return channel;
        }
        public EndpointAddress CreateEndPointAddress(string address)
        {
            EndpointAddress epa;
            epa = new EndpointAddress(address);
            return epa;
        }
    }
}
