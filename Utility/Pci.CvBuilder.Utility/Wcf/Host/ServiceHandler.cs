﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;
using SimpleInjector;
using SimpleInjector.Integration.Wcf;

namespace Pci.CvBuilder.Utility.Wcf.Host
{
    public class ServiceHandler
    {
        public ServiceHost CreateService(
            Type interfaceType,
            Type implemetationType,
            string address,
            Container container
            )
        {
            Uri[] adrbase = { new Uri(address) };
            ServiceHost svcHost = new SimpleInjectorServiceHost(container, implemetationType, adrbase);
            return AddTcpEndPoint(svcHost, interfaceType, address);
        }

        private ServiceHost AddTcpEndPoint(ServiceHost svcHost, Type interfaceType, string urlAddress)
        {
            NetTcpBinding tcpb = new NetTcpBinding();
            tcpb.TransferMode = TransferMode.Streamed;
            tcpb.MaxBufferSize = int.MaxValue;
            tcpb.MaxReceivedMessageSize = int.MaxValue;
            tcpb.ReaderQuotas.MaxStringContentLength = int.MaxValue;
            tcpb.ReaderQuotas.MaxArrayLength = int.MaxValue;
            tcpb.ReaderQuotas.MaxBytesPerRead = int.MaxValue;
            tcpb.Security.Mode = SecurityMode.None;
            tcpb.ReceiveTimeout = new TimeSpan(0, 30, 0);
            tcpb.SendTimeout = new TimeSpan(0, 30, 0);
            ServiceMetadataBehavior mBehave = new ServiceMetadataBehavior();
            svcHost.Description.Behaviors.Add(mBehave);
            svcHost.AddServiceEndpoint(interfaceType, tcpb, urlAddress);
            svcHost.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexTcpBinding(), "mex");
            return svcHost;
        }
    }
}
